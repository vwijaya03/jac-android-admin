# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/vikowijaya/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-ignorewarnings

-dontskipnonpubliclibraryclassmembers
-dontshrink
-dontoptimize
-printmapping build/libs/output/obfuscation.map
-keepattributes
-adaptclassstrings
-dontnote
-dontwarn

-keep class persistence.** {
  *;
}

# Keep Android classes
-keep class ** extends android.** {
    <fields>;
    <methods>;
}

# Keep serializable classes & fields
-keep class ** extends java.io.Serializable {
    <fields>;
}

# Keep - Applications. Keep all application classes, along with their 'main'
# methods.
-keep public class * {
    public static void main(java.lang.String[]);
}

# Also keep - Enumerations. Keep the special static methods that are required in
# enumeration classes.
-keep enum  * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** e(...);
    public static *** w(...);
}