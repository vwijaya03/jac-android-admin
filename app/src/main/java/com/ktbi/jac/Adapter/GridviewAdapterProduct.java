package com.ktbi.jac.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ktbi.jac.Model.ProductModel;
import com.ktbi.jac.Activity.ProductDetailActivity;
import com.ktbi.jac.R;
import com.squareup.picasso.Picasso;

import java.util.List;


//public class GridviewAdapterProduct extends BaseAdapter {

//    Context context;
//
//    List<ProductModel> bean;
////    ArrayList<CategoryModel> bean;
//
////    public GridviewAdapterProduct(Context context, ArrayList<CategoryModel> bean) {
////        this.bean = bean;
////        this.context = context;
////    }
//
//    public GridviewAdapterProduct(Context context, List<ProductModel> bean) {
//        this.bean = bean;
//        this.context = context;
//    }
//
//    @Override
//    public int getCount() {
//        return bean.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return bean.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder viewHolder = null;
//
//        if (convertView == null) {
//            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//            convertView = layoutInflater.inflate(R.layout.grid_view_product_item, null);
//
//            viewHolder = new ViewHolder();
//
//            viewHolder.image1 = (ImageView) convertView.findViewById(R.id.image1);
//            viewHolder.title1 = (TextView) convertView.findViewById(R.id.title1);
//            viewHolder.discription1 = (TextView) convertView.findViewById(R.id.description1);
//            viewHolder.date1 = (TextView) convertView.findViewById(R.id.date1);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }
//
//        final ProductModel bean = (ProductModel) getItem(position);
////        final CategoryModel bean = (CategoryModel) getItem(position);
//
//        final String value_description;
//        final String value_title;
//
//        if (bean.getDescription() == null || bean.getDescription().length() <= 0) {
//            value_description = "...";
//        } else if (bean.getDescription().length() <= 35) {
//            value_description = bean.getDescription();
//        } else {
//            value_description = bean.getDescription().substring(0, 35);
//        }
//
//        if (bean.getTitle() == null || bean.getTitle().length() <= 0) {
//            value_title = "...";
//        } else if (bean.getTitle().length() <= 10) {
//            value_title = bean.getTitle();
//        } else {
//            value_title = bean.getTitle().substring(0, 10);
//        }
//
////        Picasso.with(context).load(bean.getImg_path1()).into(viewHolder.image1);
//        viewHolder.title1.setText(bean.getTitle());
//        viewHolder.discription1.setText(value_description+"...");
//
////        viewHolder.image1.setImageResource(bean.getImage1());
////        viewHolder.date1.setText(bean.getDate1());
//
//        viewHolder.image1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Toast.makeText(context, bean.getTitle1(), Toast.LENGTH_LONG).show();
//                Intent i = new Intent(context, ProductDetailActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                view.getContext().startActivity(i);
//            }
//        });
//
//
//        return convertView;
//    }
//
//    private class ViewHolder {
//        ImageView image1;
//        TextView title1;
//        TextView discription1;
//        TextView date1;
//    }
//}

public class GridviewAdapterProduct extends RecyclerView.Adapter<GridviewAdapterProduct.ViewHolder> {

    List<ProductModel> items;

    public GridviewAdapterProduct(List<ProductModel> items){
        super();
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_product_item, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, items.get(viewHolder.getAdapterPosition()).getTitle() + " -  "+items.get(viewHolder.getAdapterPosition()).getId(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent i = new Intent(view.getContext(), ProductDetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("id", items.get(viewHolder.getAdapterPosition()).getId());
                i.putExtra("category_id", items.get(viewHolder.getAdapterPosition()).getCategory_id());
                i.putExtra("category_name", items.get(viewHolder.getAdapterPosition()).getCategory_name());
                i.putExtra("title", items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("description", items.get(viewHolder.getAdapterPosition()).getDescription());
                i.putExtra("img_path1", items.get(viewHolder.getAdapterPosition()).getImg_path1());
                i.putExtra("img_path2", items.get(viewHolder.getAdapterPosition()).getImg_path2());
                i.putExtra("img_path3", items.get(viewHolder.getAdapterPosition()).getImg_path3());
                i.putExtra("img_path4", items.get(viewHolder.getAdapterPosition()).getImg_path4());
                view.getContext().startActivity(i);
            }
        });

        viewHolder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, items.get(viewHolder.getAdapterPosition()).getTitle() + " -  "+items.get(viewHolder.getAdapterPosition()).getId(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent i = new Intent(view.getContext(), ProductDetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("id", items.get(viewHolder.getAdapterPosition()).getId());
                i.putExtra("category_id", items.get(viewHolder.getAdapterPosition()).getCategory_id());
                i.putExtra("category_name", items.get(viewHolder.getAdapterPosition()).getCategory_name());
                i.putExtra("title", items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("description", items.get(viewHolder.getAdapterPosition()).getDescription());
                i.putExtra("img_path1", items.get(viewHolder.getAdapterPosition()).getImg_path1());
                i.putExtra("img_path2", items.get(viewHolder.getAdapterPosition()).getImg_path2());
                i.putExtra("img_path3", items.get(viewHolder.getAdapterPosition()).getImg_path3());
                i.putExtra("img_path4", items.get(viewHolder.getAdapterPosition()).getImg_path4());
                view.getContext().startActivity(i);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductModel list = items.get(position);
        Context context = holder.image.getContext();
        Picasso.with(context).load(list.getImg_path1()).into(holder.image);
        holder.title.setText(list.getTitle());
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView image;
        public TextView title;
        public TextView description;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image1);
            title = (TextView) itemView.findViewById(R.id.title1);
        }

    }
}