package com.ktbi.jac.Adapter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ktbi.jac.Model.UserModel;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Activity.UserActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vikowijaya on 9/9/17.
 */

public class ListviewAdapterUser extends RecyclerView.Adapter<ListviewAdapterUser.ViewHolder> {

    private List<UserModel> items;
    private String sInfo, sUserId, sURL, sIsApproved, sHeaderTipe;
    private View v;
    private SharedPreferences preferences;

    public ListviewAdapterUser(List<UserModel> items, String isApproved){
        super();
        this.items = items;
        this.sIsApproved = isApproved;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_user_item, parent, false);
        preferences = v.getContext().getSharedPreferences("AuthUser", MODE_PRIVATE);

        final ViewHolder viewHolder = new ViewHolder(v);

        if(sIsApproved.equalsIgnoreCase("ya"))
        {
            viewHolder.btnApprove.setVisibility(View.GONE);
            viewHolder.btnReject.setVisibility(View.GONE);
            viewHolder.btnDelete.setVisibility(View.VISIBLE);
        }

        viewHolder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setTitle("Info");
                alertDialog.setMessage("Apakah anda yakin ?");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sUserId = items.get(viewHolder.getAdapterPosition()).getId();
                        sURL = Config.POST_REJECT_User;
                        new postApproveOrRejectData().execute();
                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                alertDialog.show();
            }
        });

        viewHolder.btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setTitle("Info");
                alertDialog.setMessage("Akan anda terima sebagai ?");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("Admin", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sUserId = items.get(viewHolder.getAdapterPosition()).getId();
                        sURL = Config.POST_APPROVE_User;
                        sHeaderTipe = "admin";
                        new postApproveOrRejectData().execute();
                    }
                });
                alertDialog.setNegativeButton("Toko", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sUserId = items.get(viewHolder.getAdapterPosition()).getId();
                        sURL = Config.POST_APPROVE_User;
                        sHeaderTipe = "toko";
                        new postApproveOrRejectData().execute();
                    }
                });
                alertDialog.show();
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setTitle("Info");
                alertDialog.setMessage("Apakah anda yakin ?");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sUserId = items.get(viewHolder.getAdapterPosition()).getId();
                        sURL = Config.POST_DELETE_User;
                        new postApproveOrRejectData().execute();
                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                alertDialog.show();
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserModel list = items.get(position);

        holder.tvFullname.setText("Nama Lengkap: "+list.getFullname());
        holder.tvEmail.setText("Email: "+list.getEmail());
        holder.tvKota.setText("Username: "+list.getKota());
        holder.tvKecamatan.setText("Kecamatan: "+list.getKecamatan()+" / Kode Pos: "+list.getKode_pos());
        holder.tvTelepon.setText("No. Telepon: "+list.getTelepon());
        holder.tvDaftarSebagai.setText("Daftar sebagai: "+list.getTipe());
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvFullname, tvKota, tvKecamatan, tvTelepon, tvDaftarSebagai, tvEmail;
        public Button btnReject, btnApprove, btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            tvFullname = (TextView) itemView.findViewById(R.id.user_fullname);
            tvEmail = (TextView) itemView.findViewById(R.id.user_email);
            tvKota = (TextView) itemView.findViewById(R.id.user_kota);
            tvKecamatan = (TextView) itemView.findViewById(R.id.user_kecamatan);
            tvTelepon = (TextView) itemView.findViewById(R.id.user_telepon);
            tvDaftarSebagai = (TextView) itemView.findViewById(R.id.user_daftar_sebagai);
            btnApprove = (Button) itemView.findViewById(R.id.btn_approve);
            btnReject = (Button) itemView.findViewById(R.id.btn_reject);
            btnDelete = (Button) itemView.findViewById(R.id.btn_delete);
        }
    }

    private void parseJSONData(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);

            if(!jsonObject.isNull("error"))
            {
                sInfo = jsonObject.getString(Config.ERROR_TAG_JSON_ARRAY);
            }
            else if(jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY) != null)
            {
                sInfo = jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class postApproveOrRejectData extends AsyncTask<Void,Void,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(v.getContext(), "Loading", "Loading...",false,false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s == null)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }
            else
            {
                parseJSONData(s);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());

                alertDialog.setTitle("Info");
                alertDialog.setMessage(sInfo);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(v.getContext(), UserActivity.class);
                        v.getContext().startActivity(i);
                    }
                });

                alertDialog.show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(sURL+"/"+sUserId);

                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                con.addRequestProperty("tipe", sHeaderTipe);
                con.setRequestMethod("POST");

                StringBuilder sb = new StringBuilder();

                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String json = bufferedReader.readLine();

                return json.toString().trim();

            } catch (SocketTimeoutException e){
                return null;
            } catch(Exception e){
                return null;
            }
        }
    }
}
