package com.ktbi.jac.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ktbi.jac.Model.DrawerItemModel;
import com.ktbi.jac.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vikowijaya on 8/1/17.
 */

public class DrawerListAdapter extends RecyclerView.Adapter<DrawerListAdapter.DrawerViewHolder> {

    public final static int TYPE_HEADER = 0;
    public final static int TYPE_MENU = 1;


    private ArrayList<DrawerItemModel> drawerMenuList;

    private OnItemSelecteListener mListener;

    public DrawerListAdapter(ArrayList<DrawerItemModel> drawerMenuList) {
        super();
        this.drawerMenuList = drawerMenuList;
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_drawer_item, parent, false);

//        final DrawerViewHolder viewHolder = new DrawerViewHolder(v);
        final DrawerViewHolder viewHolder = new DrawerViewHolder(v, viewType);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(DrawerViewHolder holder, int position) {

        DrawerItemModel dim = drawerMenuList.get(position);

        holder.title.setText(dim.getTitle());
        holder.icon.setImageResource(dim.getIcon());
    }

    @Override
    public int getItemCount() {
        return drawerMenuList.size();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        ImageView icon;

        public DrawerViewHolder(View itemView, int viewType) {
            super(itemView);


            title = (TextView) itemView.findViewById(R.id.title);
            icon = (ImageView) itemView.findViewById(R.id.icon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemSelected(view, getAdapterPosition());

                }
            });
        }

    }

    public void setOnItemClickLister(OnItemSelecteListener mListener) {
        this.mListener = mListener;
    }

    public interface OnItemSelecteListener{
        public void onItemSelected(View v, int position);
    }

}
