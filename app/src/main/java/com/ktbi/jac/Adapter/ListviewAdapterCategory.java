package com.ktbi.jac.Adapter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ktbi.jac.Activity.CategoryActivity;
import com.ktbi.jac.Activity.EditCategoryActivity;
import com.ktbi.jac.Activity.ProductDetailActivity;
import com.ktbi.jac.Activity.UserActivity;
import com.ktbi.jac.Model.CategoryProductModel;
import com.ktbi.jac.Model.UserModel;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vikowijaya on 9/9/17.
 */

public class ListviewAdapterCategory extends RecyclerView.Adapter<ListviewAdapterCategory.ViewHolder> {

    private List<CategoryProductModel> items;
    private String sInfo, sUserId, sURL;
    private View v;
    private SharedPreferences preferences;

    public ListviewAdapterCategory(List<CategoryProductModel> items){
        super();
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_category_item, parent, false);
        preferences = v.getContext().getSharedPreferences("AuthUser", MODE_PRIVATE);

        final ViewHolder viewHolder = new ViewHolder(v);

        viewHolder.btn_category_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Intent i = new Intent(view.getContext(), EditCategoryActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("id", items.get(viewHolder.getAdapterPosition()).getId());
                i.putExtra("category_name", items.get(viewHolder.getAdapterPosition()).getName());
                view.getContext().startActivity(i);
            }
        });

        viewHolder.btn_category_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setTitle("Info");
                alertDialog.setMessage("Apakah anda yakin ?");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sUserId = items.get(viewHolder.getAdapterPosition()).getId();
                        sURL = Config.POST_DELETE_Category;
                        new postDeleteCategoryData().execute();
                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                alertDialog.show();
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CategoryProductModel list = items.get(position);

        holder.tvCategoryName.setText((position+1)+". "+list.getName());
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvCategoryName;
        public Button btn_category_edit, btn_category_delete, btn_category_submit_edit, btn_category_cancel_edit;

        public ViewHolder(View itemView) {
            super(itemView);

            tvCategoryName = (TextView) itemView.findViewById(R.id.tv_category_name);
            btn_category_edit = (Button) itemView.findViewById(R.id.btn_edit_category);
            btn_category_delete = (Button) itemView.findViewById(R.id.btn_delete_category);
        }
    }

    private void parseJSONData(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);

            if(!jsonObject.isNull("error"))
            {
                sInfo = jsonObject.getString(Config.ERROR_TAG_JSON_ARRAY);
            }
            else if(jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY) != null)
            {
                sInfo = jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class getCategoryData extends AsyncTask<Void,Void,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(v.getContext(), "Loading", "Loading...",false,false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s == null)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }
            else
            {
                parseJSONData(s);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());

                alertDialog.setTitle("Info");
                alertDialog.setMessage(sInfo);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(v.getContext(), UserActivity.class);
                        v.getContext().startActivity(i);
                    }
                });

                alertDialog.show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(sURL+"/"+sUserId);

                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                con.setRequestMethod("POST");

                StringBuilder sb = new StringBuilder();

                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String json = bufferedReader.readLine();

                return json.toString().trim();

            } catch (SocketTimeoutException e){
                return null;
            } catch(Exception e){
                return null;
            }
        }
    }

    class postDeleteCategoryData extends AsyncTask<Void,Void,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(v.getContext(), "Loading", "Loading...",false,false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s == null)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }
            else
            {
                parseJSONData(s);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());

                alertDialog.setTitle("Info");
                alertDialog.setMessage(sInfo);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(v.getContext(), CategoryActivity.class);
                        v.getContext().startActivity(i);
                    }
                });

                alertDialog.show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(sURL+"/"+sUserId);

                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                con.setRequestMethod("POST");

                StringBuilder sb = new StringBuilder();

                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String json = bufferedReader.readLine();

                return json.toString().trim();

            } catch (SocketTimeoutException e){
                return null;
            } catch(Exception e){
                return null;
            }
        }
    }
}