package com.ktbi.jac.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.ktbi.jac.PagerAdapter.PagerAdapterCategory;
import com.ktbi.jac.PagerAdapter.PagerAdapterUser;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Logout;

/**
 * Created by vikowijaya on 9/9/17.
 */

public class CategoryActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BaseSliderView.OnSliderClickListener {

    private Toolbar mToolbar;
    private FloatingActionButton fab_category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        SharedPreferences preferences = getSharedPreferences("AuthUser",MODE_PRIVATE);

        fab_category = (FloatingActionButton) findViewById(R.id.fab_category);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        if(!preferences.getString("AuthUserTipe", "").equalsIgnoreCase("admin"))
        {
            Menu menu = navigationView.getMenu();
            MenuItem target1 = menu.findItem(R.id.nav_data_kategori);
            target1.setVisible(false);
            MenuItem target2 = menu.findItem(R.id.nav_data_user);
            target2.setVisible(false);
            fab_category.setVisibility(View.GONE);
        }

        fab_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CategoryActivity.this, AddCategoryActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        TextView tvNavHeaderEmail = (TextView) header.findViewById(R.id.nav_header_email);
        TextView tvNavHeaderUsername = (TextView) header.findViewById(R.id.nav_header_username);
        TextView tvNavHeaderFullname = (TextView) header.findViewById(R.id.nav_header_fullname);
        TextView tvNavHeaderLoginAs = (TextView) header.findViewById(R.id.nav_header_tipe);

        tvNavHeaderEmail.setText(preferences.getString("AuthUserEmail", ""));
        tvNavHeaderUsername.setText("Username anda: "+preferences.getString("AuthUserUsername", ""));
        tvNavHeaderFullname.setText(preferences.getString("AuthUserFullname", ""));
        tvNavHeaderLoginAs.setText(getString(R.string.login_as)+" "+preferences.getString("AuthUserTipe", "").toUpperCase());

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout_category);
        tabLayout.addTab(tabLayout.newTab().setText(""));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setVisibility(View.GONE);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager_category);

        PagerAdapterCategory adapter = new PagerAdapterCategory(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_change_password) {
            Intent i = new Intent(CategoryActivity.this, ChangePasswordActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_data_produk) {
            Intent i = new Intent(getApplicationContext(), ProductActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
            preferences2.edit().clear().apply();
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_pembayaran) {
            Intent i = new Intent(getApplicationContext(), PaymentActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_retur) {
            Intent i = new Intent(getApplicationContext(), ReturActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_logout) {
            new Logout(CategoryActivity.this).execute();
        } else if (id == R.id.nav_data_user) {
            Intent i = new Intent(getApplicationContext(), UserActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_kategori) {
            Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
