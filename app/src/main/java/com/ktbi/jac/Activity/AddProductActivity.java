package com.ktbi.jac.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.ktbi.jac.Adapter.GridviewAdapterProduct;
import com.ktbi.jac.Model.CategoryProductModel;
import com.ktbi.jac.Model.ProductModel;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.Logout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by vikowijaya on 9/7/17.
 */

public class AddProductActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private MenuItem saveItem;
    private EditText etAddProductTitle, etAddProductDescription;
    private TextView tvSubmitAddProduct;
    private ImageView ivImageProduct1, ivImageProduct2, ivImageProduct3, ivImageProduct4;
    private Spinner spinnerCategoryProduct;
    private ProgressDialog progressDialog;

    private ArrayList<CategoryProductModel> arrayListCategoryProduct = new ArrayList<CategoryProductModel>();
    private ArrayAdapter<CategoryProductModel> arrayAdapterCategoryProduct;

    private String sCategoryProduct, sDescriptionProduct, sTitleProduct, sSuccess, sPathImage1, sPathImage2, sPathImage3, sPathImage4, sMimeTypeImage1, sMimeTypeImage2, sMimeTypeImage3, sMimeTypeImage4;
    private static final int CAMERA_REQUEST_1_Camera = 1886;
    private static final int CAMERA_REQUEST_1_Gallery = 2886;
    private static final int CAMERA_REQUEST_2_Camera = 1887;
    private static final int CAMERA_REQUEST_2_Gallery = 2887;
    private static final int CAMERA_REQUEST_3_Camera = 1888;
    private static final int CAMERA_REQUEST_3_Gallery = 2888;
    private static final int CAMERA_REQUEST_4_Camera = 1889;
    private static final int CAMERA_REQUEST_4_Gallery = 2889;
    private static final int REQUEST_CODE_ASK_CAMERA_PERMISSION = 3000;

    private Uri mUriImage1, mUriImage2, mUriImage3, mUriImage4;

    private AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        SharedPreferences preferences = getSharedPreferences("AuthUser",MODE_PRIVATE);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        Menu menu = navigationView.getMenu();
        if(!preferences.getString("AuthUserTipe", "").equalsIgnoreCase("admin"))
        {
            MenuItem target1 = menu.findItem(R.id.nav_data_kategori);
            target1.setVisible(false);
            MenuItem target2 = menu.findItem(R.id.nav_data_user);
            target2.setVisible(false);
        }

        TextView tvNavHeaderEmail = (TextView) header.findViewById(R.id.nav_header_email);
        TextView tvNavHeaderUsername = (TextView) header.findViewById(R.id.nav_header_username);
        TextView tvNavHeaderFullname = (TextView) header.findViewById(R.id.nav_header_fullname);
        TextView tvNavHeaderLoginAs = (TextView) header.findViewById(R.id.nav_header_tipe);
        etAddProductTitle = (EditText) findViewById(R.id.et_product_title);
        etAddProductDescription = (EditText) findViewById(R.id.et_product_description);
        tvSubmitAddProduct = (TextView) findViewById(R.id.tv_submit_add_product);
        spinnerCategoryProduct = (Spinner) findViewById(R.id.spinner_category_product);
        ivImageProduct1 = (ImageView) findViewById(R.id.img_product1);
        ivImageProduct2 = (ImageView) findViewById(R.id.img_product2);
        ivImageProduct3 = (ImageView) findViewById(R.id.img_product3);
        ivImageProduct4 = (ImageView) findViewById(R.id.img_product4);

        awesomeValidation.addValidation(etAddProductTitle, RegexTemplate.NOT_EMPTY, getString(R.string.add_product_title_error));
        awesomeValidation.addValidation(etAddProductDescription, RegexTemplate.NOT_EMPTY, getString(R.string.add_product_description_error));

        tvNavHeaderEmail.setText(preferences.getString("AuthUserEmail", ""));
        tvNavHeaderUsername.setText("Username anda: "+preferences.getString("AuthUserUsername", ""));
        tvNavHeaderFullname.setText(preferences.getString("AuthUserFullname", ""));
        tvNavHeaderLoginAs.setText(getString(R.string.login_as)+" "+preferences.getString("AuthUserTipe", "").toUpperCase());

        arrayListCategoryProduct.add(new CategoryProductModel("default", "Pilih Kategori Produk"));
        arrayAdapterCategoryProduct = new ArrayAdapter<CategoryProductModel>(AddProductActivity.this, android.R.layout.simple_dropdown_item_1line, arrayListCategoryProduct);

        spinnerCategoryProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sCategoryProduct = arrayListCategoryProduct.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ivImageProduct1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddProductActivity.this);
                builder.setMessage("Ambil Gambar Dari")
                        .setCancelable(true)
                        .setPositiveButton("Kamera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                if (!checkCameraPermission()) {
                                    requestCameraPermission();
                                }

                                try {
                                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DCIM");
                                    if (!file.exists()) {
                                        file.mkdirs();
                                    }

                                    File localFile = new File(file + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                                    mUriImage1 = Uri.fromFile(localFile);

                                    Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");

                                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mUriImage1);

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        cameraIntent.setClipData(ClipData.newRawUri(null, Uri.fromFile(localFile)));
                                    }

                                    startActivityForResult(cameraIntent, CAMERA_REQUEST_1_Camera);
                                } catch (Exception localException) {
                                    Toast.makeText(AddProductActivity.this, "Exception:" + localException, Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("Galeri", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!checkCameraPermission()) {
                                    requestCameraPermission();
                                }

                                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                startActivityForResult(i, CAMERA_REQUEST_1_Gallery);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        ivImageProduct2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddProductActivity.this);
                builder.setMessage("Ambil Gambar Dari")
                        .setCancelable(true)
                        .setPositiveButton("Kamera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!checkCameraPermission()) {
                                    requestCameraPermission();
                                }

                                try {
                                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DCIM");
                                    if (!file.exists()) {
                                        file.mkdirs();
                                    }

                                    File localFile = new File(file + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                                    mUriImage2 = Uri.fromFile(localFile);

                                    Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");

                                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mUriImage2);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        cameraIntent.setClipData(ClipData.newRawUri(null, Uri.fromFile(localFile)));
                                    }

                                    startActivityForResult(cameraIntent, CAMERA_REQUEST_2_Camera);
                                } catch (Exception localException) {
                                    Toast.makeText(AddProductActivity.this, "Exception:" + localException, Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("Galeri", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!checkCameraPermission()) {
                                    requestCameraPermission();
                                }

                                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                startActivityForResult(i, CAMERA_REQUEST_2_Gallery);

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        ivImageProduct3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddProductActivity.this);
                builder.setMessage("Ambil Gambar Dari")
                        .setCancelable(true)
                        .setPositiveButton("Kamera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!checkCameraPermission()) {
                                    requestCameraPermission();
                                }

                                try {
                                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DCIM");
                                    if (!file.exists()) {
                                        file.mkdirs();
                                    }

                                    File localFile = new File(file + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                                    mUriImage3 = Uri.fromFile(localFile);

                                    Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");

                                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mUriImage3);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        cameraIntent.setClipData(ClipData.newRawUri(null, Uri.fromFile(localFile)));
                                    }

                                    startActivityForResult(cameraIntent, CAMERA_REQUEST_3_Camera);
                                } catch (Exception localException) {
                                    Toast.makeText(AddProductActivity.this, "Exception:" + localException, Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("Galeri", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!checkCameraPermission()) {
                                    requestCameraPermission();
                                }

                                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                startActivityForResult(i, CAMERA_REQUEST_3_Gallery);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        ivImageProduct4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddProductActivity.this);
                builder.setMessage("Ambil Gambar Dari")
                        .setCancelable(true)
                        .setPositiveButton("Kamera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!checkCameraPermission()) {
                                    requestCameraPermission();
                                }

                                try {
                                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DCIM");
                                    if (!file.exists()) {
                                        file.mkdirs();
                                    }

                                    File localFile = new File(file + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                                    mUriImage4 = Uri.fromFile(localFile);

                                    Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");

                                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mUriImage4);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        cameraIntent.setClipData(ClipData.newRawUri(null, Uri.fromFile(localFile)));
                                    }

                                    startActivityForResult(cameraIntent, CAMERA_REQUEST_4_Camera);
                                } catch (Exception localException) {
                                    Toast.makeText(AddProductActivity.this, "Exception:" + localException, Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("Galeri", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!checkCameraPermission()) {
                                    requestCameraPermission();
                                }

                                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                startActivityForResult(i, CAMERA_REQUEST_4_Gallery);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        tvSubmitAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sCategoryProduct.equalsIgnoreCase("default") || sCategoryProduct == "default")
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddProductActivity.this);
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Kategori produk belum ada yang dipilih !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    alertDialog.show();
                }
                else if(awesomeValidation.validate())
                {
                    sTitleProduct = etAddProductTitle.getText().toString();
                    sDescriptionProduct = etAddProductDescription.getText().toString();
                    new postAddProduct().execute();
                }
            }
        });

        if (!this.checkCameraPermission()) {
            this.requestCameraPermission();
        }

        getCategory();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        String path1 = null, path2=null, path3=null, path4=null;
        Uri uri1, uri2 = null, uri3, uri4;

        if (requestCode == CAMERA_REQUEST_1_Camera && resultCode == Activity.RESULT_OK) {

            if (intent == null || intent.getData() == null){
                uri1 = this.mUriImage1;
            }
            else{
                uri1 = intent.getData();
            }

            if(requestCode == 0) {
                path1 = getRealPathFromURI(uri1);
            } else if(requestCode == CAMERA_REQUEST_1_Camera){
                path1 = uri1.getEncodedPath();

                String mimeType = null;
                if (uri1.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                    ContentResolver cr = getApplication().getContentResolver();
                    mimeType = cr.getType(uri1);
                } else {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri1.toString());
                    mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                            fileExtension.toLowerCase());
                }
                sMimeTypeImage1 = mimeType;

            }

            ivImageProduct1.setImageBitmap(scaleToActualAspectRatio(BitmapFactory.decodeFile(path1)));
            sPathImage1 = saveResizedImageAfterTakenFromCameraToInternalStorage(scaleToActualAspectRatio(BitmapFactory.decodeFile(path1)));
        }

        else if(requestCode == CAMERA_REQUEST_1_Gallery && resultCode == Activity.RESULT_OK)
        {
            if (intent == null || intent.getData() == null){
                uri1 = this.mUriImage1;
            }
            else{
                uri1 = intent.getData();
            }

            if(requestCode == 0) {
                path1 = getRealPathFromURI(uri1);
            } else if(requestCode == CAMERA_REQUEST_1_Gallery){
                path1 = uri1.getEncodedPath();

                String mimeType = null;
                if (uri1.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                    ContentResolver cr = getApplication().getContentResolver();
                    mimeType = cr.getType(uri1);
                } else {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri1.toString());
                    mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
                }

                try {
                    // Convert path ke bitmap di set ke image view
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), intent.getData());
                    sMimeTypeImage1 = mimeType;
                    ivImageProduct1.setImageBitmap(scaleToActualAspectRatio(bitmap));
                    sPathImage1 = saveResizedImageAfterTakenFromCameraToInternalStorage(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        else if(requestCode == CAMERA_REQUEST_2_Camera && resultCode == Activity.RESULT_OK)
        {
            if (intent == null || intent.getData() == null){
                uri2 = this.mUriImage2;
            }
            else{
                uri2 = intent.getData();
            }

            if(requestCode == 0) {
                path2 = getRealPathFromURI(uri2);
            } else if(requestCode == CAMERA_REQUEST_2_Camera){
                path2 = uri2.getEncodedPath();

                String mimeType2 = null;
                if (uri2.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                    ContentResolver cr = getApplication().getContentResolver();
                    mimeType2 = cr.getType(uri2);
                } else {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri2.toString());
                    mimeType2 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                            fileExtension.toLowerCase());
                }
                sMimeTypeImage2 = mimeType2;
                ivImageProduct2.setImageBitmap(scaleToActualAspectRatio(BitmapFactory.decodeFile(path2)));
                sPathImage2 = saveResizedImageAfterTakenFromCameraToInternalStorage(scaleToActualAspectRatio(BitmapFactory.decodeFile(path2)));
            }
        }
        else if(requestCode == CAMERA_REQUEST_2_Gallery && resultCode == Activity.RESULT_OK)
        {
            if (intent == null || intent.getData() == null){
                uri2 = this.mUriImage2;
            }
            else{
                uri2 = intent.getData();
            }

            if(requestCode == 0) {
                path2 = getRealPathFromURI(uri2);
            } else if(requestCode == CAMERA_REQUEST_2_Gallery){
                path2 = uri2.getEncodedPath();

                String mimeType2 = null;
                if (uri2.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                    ContentResolver cr = getApplication().getContentResolver();
                    mimeType2 = cr.getType(uri2);
                } else {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri2.toString());
                    mimeType2 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                            fileExtension.toLowerCase());
                }

                try {
                    // Convert path ke bitmap di set ke image view
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), intent.getData());
                    sMimeTypeImage2 = mimeType2;
                    ivImageProduct2.setImageBitmap(scaleToActualAspectRatio(bitmap));
                    sPathImage2 = saveResizedImageAfterTakenFromCameraToInternalStorage(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(requestCode == CAMERA_REQUEST_3_Camera && resultCode == Activity.RESULT_OK)
        {
            if (intent == null || intent.getData() == null){
                uri3 = this.mUriImage3;
            }
            else{
                uri3 = intent.getData();
            }

            if(requestCode == 0) {
                path3 = getRealPathFromURI(uri3);
            } else if(requestCode == CAMERA_REQUEST_3_Camera){
                path3 = uri3.getEncodedPath();

                String mimeType3 = null;
                if (uri3.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                    ContentResolver cr = getApplication().getContentResolver();
                    mimeType3 = cr.getType(uri3);
                } else {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri3.toString());
                    mimeType3 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                            fileExtension.toLowerCase());
                }
                sMimeTypeImage3 = mimeType3;
                ivImageProduct3.setImageBitmap(scaleToActualAspectRatio(BitmapFactory.decodeFile(path3)));
                sPathImage3 = saveResizedImageAfterTakenFromCameraToInternalStorage(scaleToActualAspectRatio(BitmapFactory.decodeFile(path3)));
            }
        }
        else if(requestCode == CAMERA_REQUEST_3_Gallery && resultCode == Activity.RESULT_OK)
        {
            if (intent == null || intent.getData() == null){
                uri3 = this.mUriImage3;
            }
            else{
                uri3 = intent.getData();
            }

            if(requestCode == 0) {
                path3 = getRealPathFromURI(uri3);
            } else if(requestCode == CAMERA_REQUEST_3_Gallery){
                path3 = uri3.getEncodedPath();

                String mimeType3 = null;
                if (uri3.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                    ContentResolver cr = getApplication().getContentResolver();
                    mimeType3 = cr.getType(uri3);
                } else {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri3.toString());
                    mimeType3 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                            fileExtension.toLowerCase());
                }

                try {
                    // Convert path ke bitmap di set ke image view
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), intent.getData());
                    sMimeTypeImage3 = mimeType3;
                    ivImageProduct3.setImageBitmap(scaleToActualAspectRatio(bitmap));
                    sPathImage3 = saveResizedImageAfterTakenFromCameraToInternalStorage(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(requestCode == CAMERA_REQUEST_4_Camera && resultCode == Activity.RESULT_OK)
        {
            if (intent == null || intent.getData() == null){
                uri4 = this.mUriImage4;
            }
            else{
                uri4 = intent.getData();
            }

            if(requestCode == 0) {
                path4 = getRealPathFromURI(uri4);
            } else if(requestCode == CAMERA_REQUEST_4_Camera){
                path4 = uri4.getEncodedPath();

                String mimeType4 = null;
                if (uri4.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                    ContentResolver cr = getApplication().getContentResolver();
                    mimeType4 = cr.getType(uri4);
                } else {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri4.toString());
                    mimeType4 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                            fileExtension.toLowerCase());
                }
                sMimeTypeImage4 = mimeType4;
                ivImageProduct4.setImageBitmap(scaleToActualAspectRatio(BitmapFactory.decodeFile(path4)));
                sPathImage4 = saveResizedImageAfterTakenFromCameraToInternalStorage(scaleToActualAspectRatio(BitmapFactory.decodeFile(path4)));
            }
        }
        else if(requestCode == CAMERA_REQUEST_4_Gallery && resultCode == Activity.RESULT_OK)
        {
            if (intent == null || intent.getData() == null){
                uri4 = this.mUriImage4;
            }
            else{
                uri4 = intent.getData();
            }

            if(requestCode == 0) {
                path4 = getRealPathFromURI(uri4);
            } else if(requestCode == CAMERA_REQUEST_4_Gallery){
                path4 = uri4.getEncodedPath();

                String mimeType4 = null;
                if (uri4.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                    ContentResolver cr = getApplication().getContentResolver();
                    mimeType4 = cr.getType(uri4);
                } else {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri4.toString());
                    mimeType4 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                            fileExtension.toLowerCase());
                }

                try {
                    // Convert path ke bitmap di set ke image view
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), intent.getData());
                    sMimeTypeImage4 = mimeType4;
                    ivImageProduct4.setImageBitmap(scaleToActualAspectRatio(bitmap));
                    sPathImage4 = saveResizedImageAfterTakenFromCameraToInternalStorage(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String saveResizedImageAfterTakenFromCameraToInternalStorage(Bitmap bitmap)
    {
        String absolutePath = "";
        // Start Buat Image Baru Yang Di Compress lalu disimpan di internal storage
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"DCIM");
        if (!file.exists()) {
            file.mkdirs();
        }

        File localFile = new File(file + File.separator + "IMG_Resized_" + String.valueOf(System.currentTimeMillis()) + ".jpg");

        try {
            OutputStream fOut = new FileOutputStream(localFile);
            scaleToActualAspectRatio(bitmap).compress(Bitmap.CompressFormat.JPEG, 80, fOut);
            fOut.flush();
            fOut.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), localFile.getAbsolutePath(), localFile.getName(), localFile.getName());
            absolutePath = localFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // END Buat Image Baru Yang Di Compress lalu disimpan di internal storage

        return absolutePath;
    }

    private Bitmap scaleToActualAspectRatio(Bitmap image) {

        Display display = getWindowManager().getDefaultDisplay();

        // display size in pixels
        Point size = new Point();
        display.getSize(size);
        int maxWidth = size.x;
        int maxHeight = size.y;

        getWindowManager().getDefaultDisplay().getWidth();

        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_CAMERA_PERMISSION: {
                if (!this.checkCameraPermission()) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddProductActivity.this);

                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Membutuhkan izin mengakses camera / galeri.");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                }
            } break;
        }
    }

    private boolean checkCameraPermission() {
        boolean camera = (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
        boolean storage = (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        return camera && storage;
    }

    private void requestCameraPermission(){
        ActivityCompat.requestPermissions(this, new String[]{
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_CAMERA_PERMISSION);
    }

    public String getRealPathFromURI(Uri uri){
        String filePath = "";
        String[] filePahColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePahColumn, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                int columnIndex = cursor.getColumnIndex(filePahColumn[0]);
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        saveItem = (MenuItem) menu.findItem(R.id.search_product_item);
        saveItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_change_password) {
            Intent i = new Intent(AddProductActivity.this, ChangePasswordActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_data_produk) {
            Intent i = new Intent(getApplicationContext(), ProductActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
            preferences2.edit().clear().apply();
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_pembayaran) {
            Intent i = new Intent(getApplicationContext(), PaymentActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_retur) {
            Intent i = new Intent(getApplicationContext(), ReturActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }  else if (id == R.id.nav_logout) {
            new Logout(AddProductActivity.this).execute();
        } else if (id == R.id.nav_data_user) {
            Intent i = new Intent(getApplicationContext(), UserActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_kategori) {
            Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void parseJSONAddProduct(String json){
        Log.i("isi json", json);
        try {
            JSONObject jsonObject = new JSONObject(json);

            if(!jsonObject.isNull("error"))
            {
                sSuccess = jsonObject.getString(Config.ERROR_TAG_JSON_ARRAY);
            }
            else if(jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY) != null)
            {
                sSuccess = jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getCategory(){
        class GetData extends AsyncTask<Void,Void,String> {
            public int page = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(AddProductActivity.this, "Loading", "Loading...",false,false);
                progressDialog.setCancelable(false);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                if(s == null)
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddProductActivity.this);

                    alertDialog.setTitle("Info");
                    alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    });

                    alertDialog.show();
                }
                else if(s.equalsIgnoreCase("401"))
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddProductActivity.this);

                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Sesi login anda sudah habis !");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser",MODE_PRIVATE);
                            preferences.edit().clear().apply();

                            Intent i = new Intent(AddProductActivity.this, UserLoginOrSignupActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        }
                    });

                    alertDialog.show();
                }
                else
                {
                    parseJSONCategory(s);
                }

            }

            @Override
            protected String doInBackground(Void... params) {
                BufferedReader bufferedReader = null;
                try {
                    URL url;
                    url = new URL(Config.GET_Category);

                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                    con.setRequestMethod("GET");

                    int response_code = con.getResponseCode();

                    if(response_code == HttpURLConnection.HTTP_UNAUTHORIZED)
                    {
                        return String.valueOf(response_code);
                    }

                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                } catch (SocketTimeoutException e){
                    return null;
                } catch(Exception e){
                    return null;
                }
            }
        }
        GetData gd = new GetData();
//        gd.page = page;
        gd.execute();
    }

    private void parseJSONCategory(String json){
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONArray arrayCategoryProduct = jsonObject.getJSONArray(Config.DEFAULT_TAG_JSON_ARRAY);

            for(int m=0; m < arrayCategoryProduct.length(); m++){
                JSONObject n = arrayCategoryProduct.getJSONObject(m);
                this.arrayListCategoryProduct.add(new CategoryProductModel(getCategoryProductId(n), getCategoryProductName(n)));
            }

            arrayAdapterCategoryProduct = new ArrayAdapter<CategoryProductModel>(AddProductActivity.this, android.R.layout.simple_dropdown_item_1line, arrayListCategoryProduct);
            spinnerCategoryProduct.setAdapter(arrayAdapterCategoryProduct);

            arrayAdapterCategoryProduct.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getCategoryProductId(JSONObject j){
        String id = null;
        try {
            id = j.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    private String getCategoryProductName(JSONObject j){
        String name = null;
        try {
            name = j.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return name;
    }

    public String multipartRequest(String urlTo, Map<String, String> parmas, String filepath, String filefield, String fileMimeType) {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;

        String twoHyphens = "--";
        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String lineEnd = "\r\n";

        String result = "";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        String[] q = filepath.split("/");
        int idx = q.length - 1;

        try {
            File file = new File(filepath);
            FileInputStream fileInputStream = new FileInputStream(file);
            Log.i("kepanggil", "terpanggil");
            URL url = new URL(Config.POST_ADD_Product);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
            connection.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
            connection.addRequestProperty("category", "1");
            connection.addRequestProperty("title", "Test Upload");
            connection.addRequestProperty("description", "Ularrrrr");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "img" + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
            outputStream.writeBytes("Content-Type: " + fileMimeType + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);

            // Upload POST Data
            Iterator<String> keys = parmas.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = parmas.get(key);

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(value);
                outputStream.writeBytes(lineEnd);
            }

            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


            if (200 != connection.getResponseCode()) {
            }

            inputStream = connection.getInputStream();

            result = this.convertStreamToString(inputStream);

            fileInputStream.close();
            inputStream.close();
            outputStream.flush();
            outputStream.close();

            return result;
        } catch (Exception e) {
            Log.e("Error COk", String.valueOf(e));
        }
        return "a";
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    class postAddProduct extends AsyncTask<Void,Void,String> {
            public int page = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(AddProductActivity.this, "Loading", "Loading...", false, false);
                progressDialog.setCancelable(false);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                if (s == null) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddProductActivity.this);

                    alertDialog.setTitle("Info");
                    alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    });

                    alertDialog.show();
                } else if (s.equalsIgnoreCase("401")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddProductActivity.this);

                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Sesi login anda sudah habis !");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
                            preferences.edit().clear().apply();

                            Intent i = new Intent(AddProductActivity.this, UserLoginOrSignupActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        }
                    });

                    alertDialog.show();
                } else {
                    parseJSONAddProduct(s);

                    ivImageProduct1.setImageResource(R.drawable.ic_photo_black_48dp);
                    ivImageProduct2.setImageResource(R.drawable.ic_photo_black_48dp);
                    ivImageProduct3.setImageResource(R.drawable.ic_photo_black_48dp);
                    ivImageProduct4.setImageResource(R.drawable.ic_photo_black_48dp);

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddProductActivity.this);
                    alertDialog.setTitle("Info");
                    alertDialog.setMessage(sSuccess);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setCancelable(false);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(getApplicationContext(), ProductActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
                            preferences2.edit().clear().apply();
                            startActivity(i);
                        }
                    });

                    alertDialog.show();
                }

            }

            @Override
            protected String doInBackground(Void... params) {
                BufferedReader bufferedReader = null;

                Map<String, String> request_parameter = new HashMap<String, String>(2);
                request_parameter.put("category", sCategoryProduct);
                request_parameter.put("title", sTitleProduct);
                request_parameter.put("description", sDescriptionProduct);

                HttpURLConnection connection = null;
                DataOutputStream outputStream = null;
                InputStream inputStream = null;

                String twoHyphens = "--";
                String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
                String lineEnd = "\r\n";

                String result = "";

                int bytesRead, bytesRead2, bytesAvailable, bytesAvailable2, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;

                try {
                    URL url = new URL(Config.POST_ADD_Product);
                    connection = (HttpURLConnection) url.openConnection();

                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.setUseCaches(false);

                    connection.setRequestMethod("POST");
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
                    connection.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
                    connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                    outputStream = new DataOutputStream(connection.getOutputStream());

                    if(sPathImage1 != null)
                    {
                        String[] q1 = sPathImage1.split("/");
                        int idx1 = q1.length - 1;

                        File file1 = new File(sPathImage1);
                        FileInputStream fileInputStream1 = new FileInputStream(file1);

                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "img_path1" + "\"; filename=\"" + q1[idx1] + "\"" + lineEnd);
                        outputStream.writeBytes("Content-Type: " + sMimeTypeImage1 + lineEnd);
                        outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

                        outputStream.writeBytes(lineEnd);

                        bytesAvailable = fileInputStream1.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        bytesRead = fileInputStream1.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            outputStream.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream1.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream1.read(buffer, 0, bufferSize);
                        }

                        outputStream.writeBytes(lineEnd);
                        fileInputStream1.close();
                    }

                    if(sPathImage2 != null)
                    {
                        String[] q2 = sPathImage2.split("/");
                        int idx2 = q2.length - 1;

                        File file2 = new File(sPathImage2);
                        FileInputStream fileInputStream2 = new FileInputStream(file2);

                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "img_path2" + "\"; filename=\"" + q2[idx2] + "\"" + lineEnd);
                        outputStream.writeBytes("Content-Type: " + sMimeTypeImage2 + lineEnd);
                        outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

                        outputStream.writeBytes(lineEnd);

                        bytesAvailable = fileInputStream2.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        bytesRead = fileInputStream2.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            outputStream.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream2.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream2.read(buffer, 0, bufferSize);
                        }

                        outputStream.writeBytes(lineEnd);
                        fileInputStream2.close();
                    }

                    if(sPathImage3 != null)
                    {
                        String[] q3 = sPathImage3.split("/");
                        int idx3 = q3.length - 1;

                        File file3 = new File(sPathImage3);
                        FileInputStream fileInputStream3 = new FileInputStream(file3);

                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "img_path3" + "\"; filename=\"" + q3[idx3] + "\"" + lineEnd);
                        outputStream.writeBytes("Content-Type: " + sMimeTypeImage3 + lineEnd);
                        outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

                        outputStream.writeBytes(lineEnd);

                        bytesAvailable = fileInputStream3.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        bytesRead = fileInputStream3.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            outputStream.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream3.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream3.read(buffer, 0, bufferSize);
                        }

                        outputStream.writeBytes(lineEnd);
                        fileInputStream3.close();
                    }

                    if(sPathImage4 != null)
                    {
                        String[] q4 = sPathImage4.split("/");
                        int idx4 = q4.length - 1;

                        File file4 = new File(sPathImage4);
                        FileInputStream fileInputStream4 = new FileInputStream(file4);

                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "img_path4" + "\"; filename=\"" + q4[idx4] + "\"" + lineEnd);
                        outputStream.writeBytes("Content-Type: " + sMimeTypeImage4 + lineEnd);
                        outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

                        outputStream.writeBytes(lineEnd);

                        bytesAvailable = fileInputStream4.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        bytesRead = fileInputStream4.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            outputStream.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream4.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream4.read(buffer, 0, bufferSize);
                        }

                        outputStream.writeBytes(lineEnd);
                        fileInputStream4.close();
                    }

                    // Upload POST Data
                    Iterator<String> keys = request_parameter.keySet().iterator();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        String value = request_parameter.get(key);

                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                        outputStream.writeBytes(lineEnd);
                        outputStream.writeBytes(value);
                        outputStream.writeBytes(lineEnd);
                    }

                    outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


                    if (200 != connection.getResponseCode()) {
                    }

                    inputStream = connection.getInputStream();

                    result = convertStreamToString(inputStream);

                    inputStream.close();
                    outputStream.flush();
                    outputStream.close();

                    return result.trim().toString();
                } catch (Exception e) {
                    return null;
                }
            }

        }
    }