package com.ktbi.jac.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.MCrypt;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

public class SignupActivity extends AppCompatActivity {
    private TextView signinhere, tvSubmitRegister;
    private Typeface fonts1;
    private EditText etFullname, etEmail, etUsername, etPassword, etTelepon, etKota, etKecamatan, etKodePos, etTipe;
    private AwesomeValidation awesomeValidation;
    private MCrypt mCrypt;

    private String sSuccess, sPassword = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        TextView t1 =(TextView) findViewById(R.id.signinhere);
        signinhere = (TextView) findViewById(R.id.signinhere);

        //etFullname = (EditText) findViewById(R.id.fullname);
        etEmail = (EditText) findViewById(R.id.email);
        etUsername = (EditText) findViewById(R.id.username);
        etPassword = (EditText) findViewById(R.id.password);
        //etTelepon = (EditText) findViewById(R.id.telepon);
        //etKota = (EditText) findViewById(R.id.kota);
        //etKecamatan = (EditText) findViewById(R.id.kecamatan);
        //etKodePos = (EditText) findViewById(R.id.kode_pos);
        tvSubmitRegister = (TextView) findViewById(R.id.submit_register);

        signinhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SignupActivity.this, SigninActivity.class);
                startActivity(it);
            }
        });

        fonts1 =  Typeface.createFromAsset(SignupActivity.this.getAssets(), "fonts/Lato-Regular.ttf");
        t1.setTypeface(fonts1);

        //awesomeValidation.addValidation(etFullname, RegexTemplate.NOT_EMPTY, getString(R.string.fullname_error));
//        awesomeValidation.addValidation(etEmail, RegexTemplate.NOT_EMPTY, getString(R.string.email_error));
        awesomeValidation.addValidation(etUsername, RegexTemplate.NOT_EMPTY, getString(R.string.username_error));
        awesomeValidation.addValidation(etPassword, RegexTemplate.NOT_EMPTY, getString(R.string.password_error));
        //awesomeValidation.addValidation(etTelepon, RegexTemplate.NOT_EMPTY, getString(R.string.telepon_error));
        //awesomeValidation.addValidation(etKota, RegexTemplate.NOT_EMPTY, getString(R.string.kota_error));
        //awesomeValidation.addValidation(etKecamatan, RegexTemplate.NOT_EMPTY, getString(R.string.kecamatan_error));
        //awesomeValidation.addValidation(etKodePos, RegexTemplate.NOT_EMPTY, getString(R.string.kode_pos_error));

        tvSubmitRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(awesomeValidation.validate()){

                    mCrypt = new MCrypt();
                    try {
                        sPassword = MCrypt.bytesToHex(mCrypt.encrypt(etPassword.getText().toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    new postRegisterUser().execute(
                            "",
                            "",
                            etUsername.getText().toString(),
                            sPassword,
                            "",
                            "",
                            "",
                            ""
                    );
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    class postRegisterUser extends AsyncTask<String,Void,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(SignupActivity.this, "Loading", "Loading...",false,false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s == null)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SignupActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }
            else
            {
                parseJSON(s);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SignupActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage(sSuccess);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                        etFullname.setText("");
//                        etEmail.setText("");
//                        etUsername.setText("");
//                        etPassword.setText("");
//                        etTelepon.setText("");
//                        etKota.setText("");
//                        etKecamatan.setText("");
//                        etKodePos.setText("");
//                        etTipe.setText("");
                    }
                });

                alertDialog.show();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(Config.POST_ADD_User);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                // setDoInput and setDoOutput method depict handling of both send and receive
                con.setDoInput(true);
                con.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("fullname", "not used")
                        .appendQueryParameter("email", "not used")
                        .appendQueryParameter("username", params[2])
                        .appendQueryParameter("password", params[3])
                        .appendQueryParameter("telepon", "not used")
                        .appendQueryParameter("kota", "not used")
                        .appendQueryParameter("kecamatan", "not used")
                        .appendQueryParameter("kode_pos", "not used");

                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                con.connect();

                StringBuilder sb = new StringBuilder();

                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String json = bufferedReader.readLine();

                return json.toString().trim();

            } catch (SocketTimeoutException e){
                return null;
            } catch(Exception e){
                return null;
            }
        }
    }

    private void parseJSON(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);

            if(!jsonObject.isNull("error"))
            {
                sSuccess = jsonObject.getString(Config.ERROR_TAG_JSON_ARRAY);
            }
            else if(jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY) != null)
            {
                sSuccess = jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
