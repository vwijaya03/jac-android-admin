package com.ktbi.jac.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.Logout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Created by vikowijaya on 9/9/17.
 */

public class AddCategoryActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    private MenuItem saveItem;
    private EditText etAddCategoryName;
    private TextView tvSubmitAddCategory;
    private AwesomeValidation awesomeValidation;

    private String sInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        SharedPreferences preferences = getSharedPreferences("AuthUser",MODE_PRIVATE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        Menu menu = navigationView.getMenu();
        if(!preferences.getString("AuthUserTipe", "").equalsIgnoreCase("admin"))
        {
            MenuItem target1 = menu.findItem(R.id.nav_data_kategori);
            target1.setVisible(false);
            MenuItem target2 = menu.findItem(R.id.nav_data_user);
            target2.setVisible(false);
        }

        TextView tvNavHeaderEmail = (TextView) header.findViewById(R.id.nav_header_email);
        TextView tvNavHeaderUsername = (TextView) header.findViewById(R.id.nav_header_username);
        TextView tvNavHeaderFullname = (TextView) header.findViewById(R.id.nav_header_fullname);
        TextView tvNavHeaderLoginAs = (TextView) header.findViewById(R.id.nav_header_tipe);

        etAddCategoryName = (EditText) findViewById(R.id.et_add_category_name);
        tvSubmitAddCategory = (TextView) findViewById(R.id.tv_submit_add_category);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(etAddCategoryName, RegexTemplate.NOT_EMPTY, getString(R.string.category_name_error));

        tvNavHeaderEmail.setText(preferences.getString("AuthUserEmail", ""));
        tvNavHeaderUsername.setText("Username anda: "+preferences.getString("AuthUserUsername", ""));
        tvNavHeaderFullname.setText(preferences.getString("AuthUserFullname", ""));
        tvNavHeaderLoginAs.setText(getString(R.string.login_as)+" "+preferences.getString("AuthUserTipe", "").toUpperCase());

        tvSubmitAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Panggil API
                //getPaymentData();

                if(awesomeValidation.validate())
                {
                    new postAddCategoryName().execute(etAddCategoryName.getText().toString());
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        saveItem = (MenuItem) menu.findItem(R.id.search_product_item);
        saveItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_change_password) {
            Intent i = new Intent(AddCategoryActivity.this, ChangePasswordActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_data_produk) {
            Intent i = new Intent(getApplicationContext(), ProductActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
            preferences2.edit().clear().apply();
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_pembayaran) {
            Intent i = new Intent(getApplicationContext(), PaymentActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_retur) {
            Intent i = new Intent(getApplicationContext(), ReturActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }  else if (id == R.id.nav_logout) {
            new Logout(AddCategoryActivity.this).execute();
        } else if (id == R.id.nav_data_user) {
            Intent i = new Intent(getApplicationContext(), UserActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_kategori) {
            Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class postAddCategoryName extends AsyncTask<String,Void,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(AddCategoryActivity.this, "Loading", "Loading...",false,false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s == null)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddCategoryActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }
            else
            {
                parseJSON(s);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddCategoryActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage(sInfo);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        etAddCategoryName.setText("");
                        Intent i = new Intent(AddCategoryActivity.this, CategoryActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                });

                alertDialog.show();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(Config.POST_ADD_Category);
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                con.setRequestMethod("POST");
                // setDoInput and setDoOutput method depict handling of both send and receive
                con.setDoInput(true);
                con.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("name", params[0]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                con.connect();

                StringBuilder sb = new StringBuilder();

                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String json = bufferedReader.readLine();

                return json.toString().trim();

            } catch (SocketTimeoutException e){
                return null;
            } catch(Exception e){
                return null;
            }
        }
    }

    private void parseJSON(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);

            if(!jsonObject.isNull("error"))
            {
                sInfo = jsonObject.getString(Config.ERROR_TAG_JSON_ARRAY);
            }
            else if(jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY) != null)
            {
                sInfo = jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}