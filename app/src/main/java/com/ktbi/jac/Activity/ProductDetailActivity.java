package com.ktbi.jac.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.ktbi.jac.ChildAnimationProductDetail;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.Logout;
import com.ktbi.jac.SliderLayoutProductDetail;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class ProductDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BaseSliderView.OnSliderClickListener {
    private SliderLayoutProductDetail mDemoSlider;
    private LinearLayout llIconPlusDescription, llIconMinusDescription;
    private Toolbar mToolbar;
    private MenuItem saveItem;
    private TextView tvProductDetailCategory, tvProductDetailTitle, tvProductDetailDescription;
    private FloatingActionButton fab_edit_product, fab_delete_product;
    private ImageView ibWhatsapp;

    private String id, category_id, category_name, title, description, img_path1, img_path2, img_path3, img_path4, sProductInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail_activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        SharedPreferences preferences = getSharedPreferences("AuthUser",MODE_PRIVATE);

        if(savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();

            if(extras == null)
            {
                this.id = null;
                this.category_id = null;
                this.category_name = null;
                this.title = null;
                this.description = null;
                this.img_path1 = null;
                this.img_path2 = null;
                this.img_path3 = null;
                this.img_path4 = null;
            }
            else {
                this.id = extras.getString("id");
                this.category_id  = extras.getString("category_id");
                this.category_name = extras.getString("category_name");
                this.title = extras.getString("title");
                this.description = extras.getString("description");
                this.img_path1 = extras.getString("img_path1");
                this.img_path2 = extras.getString("img_path2");
                this.img_path3 = extras.getString("img_path3");
                this.img_path4 = extras.getString("img_path4");
            }
        }
        else{
            this.id = (String) savedInstanceState.getSerializable("id");
            this.category_id = (String) savedInstanceState.getSerializable("category_id");
            this.category_name = (String) savedInstanceState.getSerializable("category_name");
            this.title = (String) savedInstanceState.getSerializable("title");
            this.description = (String) savedInstanceState.getSerializable("description");
            this.img_path1 = (String) savedInstanceState.getSerializable("img_path1");
            this.img_path2 = (String) savedInstanceState.getSerializable("img_path2");
            this.img_path3 = (String) savedInstanceState.getSerializable("img_path3");
            this.img_path4 = (String) savedInstanceState.getSerializable("img_path4");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        TextView tvNavHeaderEmail = (TextView) header.findViewById(R.id.nav_header_email);
        TextView tvNavHeaderUsername = (TextView) header.findViewById(R.id.nav_header_username);
        TextView tvNavHeaderFullname = (TextView) header.findViewById(R.id.nav_header_fullname);
        TextView tvNavHeaderLoginAs = (TextView) header.findViewById(R.id.nav_header_tipe);

        tvProductDetailCategory = (TextView) findViewById(R.id.product_detail_category);
        tvProductDetailTitle = (TextView) findViewById(R.id.product_detail_title);
        //llIconPlusDescription = (LinearLayout)findViewById(R.id.linear_layout_icon_plus_description);
        llIconMinusDescription = (LinearLayout)findViewById(R.id.linear_layout_icon_minus_description);
        tvProductDetailDescription = (TextView)findViewById(R.id.product_detail_description);
        fab_edit_product = (FloatingActionButton) findViewById(R.id.fab_product_detail_edit);
        fab_delete_product = (FloatingActionButton) findViewById(R.id.fab_product_detail_delete);
        ibWhatsapp = (ImageView) findViewById(R.id.ib_whatsapp_product_detail);

        Menu menu = navigationView.getMenu();
        if(!preferences.getString("AuthUserTipe", "").equalsIgnoreCase("admin"))
        {
            MenuItem target1 = menu.findItem(R.id.nav_data_kategori);
            target1.setVisible(false);
            MenuItem target2 = menu.findItem(R.id.nav_data_user);
            target2.setVisible(false);
            fab_edit_product.setVisibility(View.GONE);
            fab_delete_product.setVisibility(View.GONE);
        }

        tvNavHeaderEmail.setText(preferences.getString("AuthUserEmail", ""));
        tvNavHeaderUsername.setText("Username anda: "+preferences.getString("AuthUserUsername", ""));
        tvNavHeaderFullname.setText(preferences.getString("AuthUserFullname", ""));
        tvNavHeaderLoginAs.setText(getString(R.string.login_as)+" "+preferences.getString("AuthUserTipe", "").toUpperCase());

        tvProductDetailCategory.setText(this.category_name);
        tvProductDetailTitle.setText(this.title);
        tvProductDetailDescription.setText(this.description);

//        llIconPlusDescription.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                llIconMinusDescription.setVisibility(View.VISIBLE);
//                llIconPlusDescription.setVisibility(View.GONE);
//                tvProductDetailDescription.setVisibility(View.VISIBLE);
//            }
//        });
//
//        llIconMinusDescription.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                llIconMinusDescription.setVisibility(View.GONE);
//                llIconPlusDescription.setVisibility(View.VISIBLE);
//                tvProductDetailDescription.setVisibility(View.GONE);
//            }
//        });

        fab_edit_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), EditProductActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("product_id", id);
                i.putExtra("category_id", category_id);
                startActivity(i);
            }
        });

        fab_delete_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductDetailActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Apakah anda yakin akan menghapus data ini ?");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new postDeleteProduct().execute();
                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                alertDialog.show();
            }
        });

        ibWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String text = "Produk "+title;// Replace with your message.

                    String toNumber = "+6282311311119"; // Replace with mobile phone number without +Sign or leading zeros.

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+toNumber+"&text="+text));
                    startActivity(intent);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        mDemoSlider = (SliderLayoutProductDetail) findViewById(R.id.slider);

        HashMap<String,String> file_maps = new LinkedHashMap<String, String>();

        if(img_path1 != null)
            file_maps.put("a", img_path1);

        if(img_path2 != null)
            file_maps.put("b", Config.PROTOCOL+Config.HOST+img_path2);

        if(img_path3 != null)
            file_maps.put("c", Config.PROTOCOL+Config.HOST+img_path3);

        if(img_path4 != null)
            file_maps.put("d", Config.PROTOCOL+Config.HOST+img_path4);

        for(String name : file_maps.keySet()){
            Log.i("isi name", name);
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayoutProductDetail
            textSliderView
                //.description(name)
                .image(file_maps.get(name))
                .setScaleType(BaseSliderView.ScaleType.CenterInside)
                .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayoutProductDetail.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayoutProductDetail.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new ChildAnimationProductDetail());
        //mDemoSlider.setDuration(4000);
        mDemoSlider.stopAutoCycle();
        mDemoSlider.addOnPageChangeListener(this);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            Intent i = new Intent(getApplicationContext(), ProductActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
//            preferences2.edit().clear().apply();
//            startActivity(i);
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        saveItem = (MenuItem) menu.findItem(R.id.search_product_item);
        saveItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_change_password) {
            Intent i = new Intent(ProductDetailActivity.this, ChangePasswordActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_data_produk) {
            Intent i = new Intent(getApplicationContext(), ProductActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
            preferences2.edit().clear().apply();
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_pembayaran) {
            Intent i = new Intent(getApplicationContext(), PaymentActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_retur) {
            Intent i = new Intent(getApplicationContext(), ReturActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_logout) {
            new Logout(ProductDetailActivity.this).execute();
        } else if (id == R.id.nav_data_user) {
            Intent i = new Intent(getApplicationContext(), UserActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_kategori) {
            Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void parseJSONDeleteProduct(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);

            if(!jsonObject.isNull("error"))
            {
                sProductInfo = jsonObject.getString(Config.ERROR_TAG_JSON_ARRAY);
            }
            else if(jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY) != null)
            {
                sProductInfo = jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class postDeleteProduct extends AsyncTask<Void,Void,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ProductDetailActivity.this, "Loading", "Loading...",false,false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s == null)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductDetailActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }
            else
            {
                parseJSONDeleteProduct(s);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductDetailActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage(sProductInfo);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(ProductDetailActivity.this, ProductActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
                        preferences2.edit().clear().apply();
                        startActivity(i);
                    }
                });

                alertDialog.show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(Config.POST_DELETE_Product+"/"+id);
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                con.setRequestMethod("POST");
                // setDoInput and setDoOutput method depict handling of both send and receive

                StringBuilder sb = new StringBuilder();

                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String json = bufferedReader.readLine();

                return json.toString().trim();

            } catch (SocketTimeoutException e){
                return null;
            } catch(Exception e){
                return null;
            }
        }
    }
}
