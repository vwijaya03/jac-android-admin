package com.ktbi.jac.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ktbi.jac.R;

public class UserLoginOrSignupActivity extends AppCompatActivity {
    TextView signin;
    TextView signup;
    private final int REQUEST_CODE_PERMISSION = 4005;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_login_or_signup_activity);

        signin = (TextView)findViewById(R.id.signin);
        signup = (TextView)findViewById(R.id.Signup);

        SharedPreferences preferences = getSharedPreferences("AuthUser",MODE_PRIVATE);
        String AuthToken = preferences.getString("AuthToken", "");

        if(!TextUtils.isEmpty(AuthToken))
        {
            Intent i = new Intent(getApplicationContext(), ReturActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
            preferences2.edit().clear().apply();
            startActivity(i);
        }

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(UserLoginOrSignupActivity.this, SignupActivity.class);
                startActivity(it);
            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(UserLoginOrSignupActivity.this,SigninActivity.class);
                startActivity(it);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!this.checkPermission()) {
            this.requestPermission();
        }
    }

    private boolean checkPermission() {
        boolean camera = (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
        boolean read_storage = (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        boolean write_storage = (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

        return camera && read_storage && write_storage;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,}, REQUEST_CODE_PERMISSION);
    }
}
