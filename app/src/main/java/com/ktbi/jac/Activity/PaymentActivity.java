package com.ktbi.jac.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.Logout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Created by vikowijaya on 9/7/17.
 */

public class PaymentActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    private MenuItem saveItem;
    private TextView tvPaymentDescription, tvEditPayment, tvDeletePayment, tvSubmitEditPayment, tvCancelEditPayment;
    private EditText etPaymentDescription;
    private FloatingActionButton fab_payment;
    private AwesomeValidation awesomeValidation;

    private String sPaymentDescription, sPaymentInfo, sResultPaymentId, sResultPaymentDescription = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        SharedPreferences preferences = getSharedPreferences("AuthUser",MODE_PRIVATE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        TextView tvNavHeaderEmail = (TextView) header.findViewById(R.id.nav_header_email);
        TextView tvNavHeaderUsername = (TextView) header.findViewById(R.id.nav_header_username);
        TextView tvNavHeaderFullname = (TextView) header.findViewById(R.id.nav_header_fullname);
        TextView tvNavHeaderLoginAs = (TextView) header.findViewById(R.id.nav_header_tipe);
        tvPaymentDescription = (TextView) findViewById(R.id.tv_payment_description);
        fab_payment = (FloatingActionButton) findViewById(R.id.fab_payment);
        tvEditPayment = (TextView) findViewById(R.id.edit_payment);
        tvDeletePayment = (TextView) findViewById(R.id.delete_payment);
        tvSubmitEditPayment = (TextView) findViewById(R.id.submit_edit_payment);
        tvCancelEditPayment = (TextView) findViewById(R.id.tv_cancel_edit_payment);
        etPaymentDescription = (EditText) findViewById(R.id.et_payment_description);

        Menu menu = navigationView.getMenu();
        if(!preferences.getString("AuthUserTipe", "").equalsIgnoreCase("admin"))
        {
            MenuItem target1 = menu.findItem(R.id.nav_data_kategori);
            target1.setVisible(false);
            MenuItem target2 = menu.findItem(R.id.nav_data_user);
            target2.setVisible(false);
            tvEditPayment.setVisibility(View.INVISIBLE);
            tvDeletePayment.setVisibility(View.INVISIBLE);
            fab_payment.setVisibility(View.INVISIBLE);
        }

        fab_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PaymentActivity.this, AddPaymentActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        tvEditPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fab_payment.setVisibility(View.GONE);
                tvDeletePayment.setVisibility(View.GONE);
                tvEditPayment.setVisibility(View.GONE);
                tvPaymentDescription.setVisibility(View.GONE);

                etPaymentDescription.setText(sResultPaymentDescription);
                etPaymentDescription.setVisibility(View.VISIBLE);
                tvSubmitEditPayment.setVisibility(View.VISIBLE);
                tvCancelEditPayment.setVisibility(View.VISIBLE);
            }
        });

        tvCancelEditPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fab_payment.setVisibility(View.VISIBLE);
                tvDeletePayment.setVisibility(View.VISIBLE);
                tvEditPayment.setVisibility(View.VISIBLE);
                tvPaymentDescription.setVisibility(View.VISIBLE);

                etPaymentDescription.setText("");
                etPaymentDescription.setVisibility(View.GONE);
                tvSubmitEditPayment.setVisibility(View.GONE);
                tvCancelEditPayment.setVisibility(View.GONE);
            }
        });

        tvDeletePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Apakah anda yakin akan menghapus data ini ?");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        postDeletePaymentData();
                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                alertDialog.show();
            }
        });

        tvSubmitEditPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Apakah anda yakin akan mengubah data ini ?");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new postEditPaymentData().execute(etPaymentDescription.getText().toString());
                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                alertDialog.show();
            }
        });

        tvNavHeaderEmail.setText(preferences.getString("AuthUserEmail", ""));
        tvNavHeaderUsername.setText("Username anda: "+preferences.getString("AuthUserUsername", ""));
        tvNavHeaderFullname.setText(preferences.getString("AuthUserFullname", ""));
        tvNavHeaderLoginAs.setText(getString(R.string.login_as)+" "+preferences.getString("AuthUserTipe", "").toUpperCase());

        // Panggil API
        getPaymentData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        saveItem = (MenuItem) menu.findItem(R.id.search_product_item);
        saveItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_change_password) {
            Intent i = new Intent(PaymentActivity.this, ChangePasswordActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_data_produk) {
            Intent i = new Intent(getApplicationContext(), ProductActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SharedPreferences preferences2 = getApplicationContext().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
            preferences2.edit().clear().apply();
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_pembayaran) {
            Intent i = new Intent(getApplicationContext(), PaymentActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_panduan_retur) {
            Intent i = new Intent(getApplicationContext(), ReturActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_logout) {
            new Logout(PaymentActivity.this).execute();
        } else if (id == R.id.nav_data_user) {
            Intent i = new Intent(getApplicationContext(), UserActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (id == R.id.nav_data_kategori) {
            Intent i = new Intent(getApplicationContext(), CategoryActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getPaymentData(){
        class getPaymentData extends AsyncTask<Void,Void,String> {
            ProgressDialog progressDialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(PaymentActivity.this, "Loading", "Loading...",false,false);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                if(s == null)
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentActivity.this);

                    alertDialog.setTitle("Info");
                    alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }
                    });

                    alertDialog.show();
                }
                else
                {
                    parseJSON(s);
                    tvPaymentDescription.setText(sPaymentDescription);
                }

            }

            @Override
            protected String doInBackground(Void... params) {
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(Config.GET_Payment);
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                    con.setRequestMethod("GET");

                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json = bufferedReader.readLine();

                    return json.toString().trim();

                } catch (SocketTimeoutException e){
                    return null;
                } catch(Exception e){
                    return null;
                }
            }
        }

        getPaymentData gpd = new getPaymentData();
        gpd.execute();
    }

    private void parseJSON(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject obj = jsonObject.getJSONObject(Config.DEFAULT_TAG_JSON_ARRAY);

            sPaymentDescription = obj.getString("description");
            sResultPaymentId = obj.getString("id");
            sResultPaymentDescription = sPaymentDescription;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void postDeletePaymentData(){
        class postDeletePaymentData extends AsyncTask<Void,Void,String> {
            ProgressDialog progressDialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(PaymentActivity.this, "Loading", "Loading...",false,false);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                if(s == null)
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentActivity.this);

                    alertDialog.setTitle("Info");
                    alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }
                    });

                    alertDialog.show();
                }
                else
                {
                    parseJSONDeletePaymentData(s);

                    tvPaymentDescription.setText("");
                    sPaymentDescription = "";
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentActivity.this);

                    alertDialog.setTitle("Info");
                    alertDialog.setMessage(sPaymentInfo);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    alertDialog.show();
                }

            }

            @Override
            protected String doInBackground(Void... params) {
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(Config.POST_DELETE_Payment+"/"+sResultPaymentId);
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                    con.setRequestMethod("POST");

                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json = bufferedReader.readLine();

                    return json.toString().trim();

                } catch (SocketTimeoutException e){
                    return null;
                } catch(Exception e){
                    return null;
                }
            }
        }

        postDeletePaymentData gdpd = new postDeletePaymentData();
        gdpd.execute();
    }

    private void parseJSONDeletePaymentData(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);

            if(!jsonObject.isNull("error"))
            {
                sPaymentInfo = jsonObject.getString(Config.ERROR_TAG_JSON_ARRAY);
            }
            else if(jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY) != null)
            {
                sPaymentInfo = jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY);
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class postEditPaymentData extends AsyncTask<String,Void,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(PaymentActivity.this, "Loading", "Loading...",false,false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s == null)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }
            else
            {
                parseJSONDeletePaymentData(s);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage(sPaymentInfo);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(PaymentActivity.this, PaymentActivity.class);
                        startActivity(i);
                    }
                });

                alertDialog.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(Config.POST_EDIT_Payment+"/"+sResultPaymentId);
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("AuthUser", MODE_PRIVATE);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                con.setRequestMethod("POST");
                // setDoInput and setDoOutput method depict handling of both send and receive
                con.setDoInput(true);
                con.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("description", params[0]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                con.connect();

                StringBuilder sb = new StringBuilder();

                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String json = bufferedReader.readLine();

                return json.toString().trim();

            } catch (SocketTimeoutException e){
                return null;
            } catch(Exception e){
                return null;
            }
        }
    }
}