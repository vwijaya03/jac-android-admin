package com.ktbi.jac.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.MCrypt;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Created by vikowijaya on 9/12/17.
 */

public class ForgotPasswordActivity extends AppCompatActivity {
    TextView create;
    Typeface fonts1;
    TextView btnSendForgotPassword, tvForgotPasswordSignIn;
    private EditText etEmailForgotPassword;
    private Context context = this;
    private AwesomeValidation awesomeValidation;

    private String sInfo;
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        etEmailForgotPassword = (EditText) findViewById(R.id.etForgotPasswordEmail);
        tvForgotPasswordSignIn = (TextView) findViewById(R.id.tv_forgot_password_sign_in);
        btnSendForgotPassword = (TextView) findViewById(R.id.btn_send_forgot_password);

        awesomeValidation.addValidation(etEmailForgotPassword, RegexTemplate.NOT_EMPTY, getString(R.string.email_error));

        fonts1 =  Typeface.createFromAsset(ForgotPasswordActivity.this.getAssets(), "fonts/Lato-Regular.ttf");

        TextView t4 =(TextView) findViewById(R.id.tv_forgot_password_sign_in);
        t4.setTypeface(fonts1);

        tvForgotPasswordSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ForgotPasswordActivity.this, SigninActivity.class);
                startActivity(i);
            }
        });

        btnSendForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(awesomeValidation.validate())
                {
                    new postResetPassword().execute(etEmailForgotPassword.getText().toString());
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    class postResetPassword extends AsyncTask<String,Void,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ForgotPasswordActivity.this, "Loading", "Loading...",false,false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s == null)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }
            else
            {
                parseJSON(s);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this);

                alertDialog.setTitle("Info");
                alertDialog.setMessage(sInfo);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        etEmailForgotPassword.setText("");
                    }
                });

                alertDialog.show();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(Config.POST_Reset_Password);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                // setDoInput and setDoOutput method depict handling of both send and receive
                con.setDoInput(true);
                con.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("email", params[0]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                con.connect();

                StringBuilder sb = new StringBuilder();

                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String json = bufferedReader.readLine();

                return json.toString().trim();

            } catch (SocketTimeoutException e){
                return null;
            } catch(Exception e){
                return null;
            }
        }
    }

    private void parseJSON(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);

            if(!jsonObject.isNull("error"))
            {
                sInfo = jsonObject.getString(Config.ERROR_TAG_JSON_ARRAY);
            }
            else if(jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY) != null)
            {
                sInfo = jsonObject.getString(Config.SUCCESS_TAG_JSON_ARRAY);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}