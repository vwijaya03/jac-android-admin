package com.ktbi.jac.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.MCrypt;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SigninActivity extends AppCompatActivity {
    TextView create;
    Typeface fonts1;
    TextView btnSignIn, btnForgotPassword;
    private EditText etUsername;
    private EditText etPassword;
    private Context context = this;
    private MCrypt mCrypt;
    private AwesomeValidation awesomeValidation;
    private SharedPreferences preferences;
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        btnSignIn = (TextView) findViewById(R.id.btn_signin);
        btnForgotPassword = (TextView) findViewById(R.id.btn_forgot_password);
        create = (TextView)findViewById(R.id.create);
        etUsername = (EditText) findViewById(R.id.username_signin);
        etPassword = (EditText) findViewById(R.id.password_signin);

        awesomeValidation.addValidation(etUsername, RegexTemplate.NOT_EMPTY, getString(R.string.username_error));
        awesomeValidation.addValidation(etPassword, RegexTemplate.NOT_EMPTY, getString(R.string.password_error));

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SigninActivity.this, SignupActivity.class);
                startActivity(it);
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if (awesomeValidation.validate()) {
                //Toast.makeText(btnSignIn.getContext(), "Validation Successfull", Toast.LENGTH_LONG).show();
                //process the data further

                mCrypt = new MCrypt();

                final String username = etUsername.getText().toString();
                final String password;
                try {
                    password = MCrypt.bytesToHex(mCrypt.encrypt(etPassword.getText().toString()));
                    new AsyncLogin().execute(username,password);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            }
        });

        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SigninActivity.this, ForgotPasswordActivity.class);
                startActivity(i);
            }
        });

        fonts1 =  Typeface.createFromAsset(SigninActivity.this.getAssets(), "fonts/Lato-Regular.ttf");

        TextView t4 =(TextView) findViewById(R.id.create);
        t4.setTypeface(fonts1);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private class AsyncLogin extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(context);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {
            try {

                // Enter URL address where your php file resides
                url = new URL(Config.POST_Login);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("username", params[0])
                        .appendQueryParameter("password", params[1]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString().trim());

                }else{
                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONObject errorObject = new JSONObject(result);

                if(errorObject.getString("error") != null)
                {
                    pdLoading.dismiss();

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(SigninActivity.this);

                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Username atau password salah !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            etUsername.setText("");
                            etPassword.setText("");
                        }
                    });

                    alertDialog.show();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //this method will be running on UI thread
            pdLoading.dismiss();

            if (result.equalsIgnoreCase("false")){

                // If username and password does not match display a error message
                Toast.makeText(context, "Invalid email or password", Toast.LENGTH_LONG).show();

            } else if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {

                Toast.makeText(context, "OOPs! Something went wrong. Connection Problem.", Toast.LENGTH_LONG).show();
            }
            else
            {
                try {

                    JSONObject resultLoginObject = new JSONObject(result);
                    JSONObject user_profile = resultLoginObject.getJSONObject(Config.DEFAULT_TAG_JSON_ARRAY);

                    preferences = getSharedPreferences("AuthUser", MODE_PRIVATE);
                    preferences.edit().putString("AuthToken", resultLoginObject.getString("token")).apply();
                    preferences.edit().putString("AuthUserId", user_profile.getString("id")).apply();
                    preferences.edit().putString("AuthUserEmail", user_profile.getString("email")).apply();
                    preferences.edit().putString("AuthUserFullname", user_profile.getString("fullname")).apply();
                    preferences.edit().putString("AuthUserUsername", user_profile.getString("username")).apply();
                    preferences.edit().putString("AuthUserTipe", user_profile.getString("tipe")).apply();

                    Intent i = new Intent(getApplicationContext(), ReturActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
