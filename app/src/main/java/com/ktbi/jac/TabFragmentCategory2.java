package com.ktbi.jac;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ktbi.jac.Adapter.GridviewAdapterProduct;
import com.ktbi.jac.Model.CategoryModel;

import java.util.ArrayList;

/**
 * Created by apple on 18/03/16.
 */
public class TabFragmentCategory2 extends Fragment {


    private ExpandableHeightGridView gridview;
    private ArrayList<CategoryModel> categoryModelArrayList;
    private GridviewAdapterProduct gridviewAdapterProduct;
    private View view;

    private int[] IMAGEgrid = {R.drawable.pik1, R.drawable.pik2, R.drawable.pik3, R.drawable.pik4, R.drawable.pik1, R.drawable.pik2,};
    private String[] TITLeGgrid = {"Min 70% off", "Min 50% off", "Min 45% off",  "Min 60% off", "Min 70% off", "Min 50% off"};
    private String[] DIscriptiongrid = {"Wrist Watches", "Belts", "Sunglasses","Perfumes", "Wrist Watches", "Belts"};
    private String[] Dategrid = {"Explore Now!","Grab Now!","Discover now!", "Great Savings!", "Explore Now!","Grab Now!"};




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product, container, false);

//        grid_view_product_item = (ExpandableHeightGridView)view.findViewById(R.id.grid_view_product_item);
//        categoryModelArrayList = new ArrayList<CategoryModel>();
//
//        for (int i= 0; i< IMAGEgrid.length; i++) {
//
//            CategoryModel categoryModel = new CategoryModel(IMAGEgrid[i], TITLeGgrid[i], DIscriptiongrid[i], Dategrid[i]);
//            categoryModelArrayList.add(categoryModel);
//
//        }
//        gridviewAdapterProduct = new GridviewAdapterProduct(getActivity(), categoryModelArrayList);
//        grid_view_product_item.setExpanded(true);
//
//        grid_view_product_item.setAdapter(gridviewAdapterProduct);
        return view;

    }
}