package com.ktbi.jac.Model;

/**
 * Created by vikowijaya on 8/1/17.
 */

public class DrawerItemModel {

    private int icon;
    private String title;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}