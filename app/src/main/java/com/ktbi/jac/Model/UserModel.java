package com.ktbi.jac.Model;

/**
 * Created by vikowijaya on 9/8/17.
 */

public class UserModel
{
    private String id;
    private String fullname;
    private String email;
    private String username;
    private String password;
    private String telepon;
    private String kota;
    private String kecamatan;
    private String kode_pos;
    private String tipe;
    private boolean isSpinner;

    public UserModel() {

    }

    public UserModel(String id, String fullname, String email, String username, String password, String telepon, String kota, String kecamatan, String kode_pos, String tipe) {
        this.id = id;
        this.fullname = fullname;
        this.email = email;
        this.username = username;
        this.password = password;
        this.telepon = telepon;
        this.kota = kota;
        this.kecamatan = kecamatan;
        this.kode_pos = kode_pos;
        this.tipe = tipe;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKode_pos() {
        return kode_pos;
    }

    public void setKode_pos(String kode_pos) {
        this.kode_pos = kode_pos;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public boolean isSpinner() {
        return isSpinner;
    }

    public void setSpinner(boolean spinner) {
        isSpinner = spinner;
    }
}
