package com.ktbi.jac.Model;

/**
 * Created by vikowijaya on 9/4/17.
 */

public class ProductModel
{
    private String id;
    private String category_id;
    private String category_name;
    private String title;
    private String description;
    private String img_path1;
    private String img_path2;
    private String img_path3;
    private String img_path4;
    private boolean isSpinner;

    public boolean isSpinner() {
        return isSpinner;
    }

    public void setSpinner(boolean spinner) {
        isSpinner = spinner;
    }

    public String getId() {
        return id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg_path1() {
        return img_path1;
    }

    public void setImg_path1(String img_path1) {
        this.img_path1 = img_path1;
    }

    public String getImg_path2() {
        return img_path2;
    }

    public void setImg_path2(String img_path2) {
        this.img_path2 = img_path2;
    }

    public String getImg_path3() {
        return img_path3;
    }

    public void setImg_path3(String img_path3) {
        this.img_path3 = img_path3;
    }

    public String getImg_path4() {
        return img_path4;
    }

    public void setImg_path4(String img_path4) {
        this.img_path4 = img_path4;
    }
}
