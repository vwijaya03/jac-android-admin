package com.ktbi.jac.Model;

import android.widget.Adapter;

/**
 * Created by vikowijaya on 9/6/17.
 */

public class CategoryProductModel {
    private String id;
    private String name;
    private boolean isSpinner;

    public boolean isSpinner() {
        return isSpinner;
    }

    public void setSpinner(boolean spinner) {
        isSpinner = spinner;
    }

    public CategoryProductModel() {
        this.id = id;
        this.name = name;
    }

    public CategoryProductModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CategoryProductModel){
            CategoryProductModel cpm = (CategoryProductModel ) obj;
            if(cpm.getName().equals(name) && cpm.getId()==id ) return true;
        }

        return false;
    }
}
