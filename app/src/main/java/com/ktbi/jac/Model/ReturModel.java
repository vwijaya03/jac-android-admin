package com.ktbi.jac.Model;

/**
 * Created by vikowijaya on 9/7/17.
 */

public class ReturModel
{
    private String id;
    private String description;

    public ReturModel()
    {

    }

    public ReturModel(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
