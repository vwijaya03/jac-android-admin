package com.ktbi.jac.Setting;

import android.content.Context;

/**
 * Created by vikowijaya on 9/2/17.
 */

public class Config
{
    public static Context mContext;

    public static final String PROTOCOL = "http://";
//    public static final String PROTOCOL = "http://";
    public static final String HOST = "156.67.219.244/";
//    public static final String HOST = "192.168.43.242/jac/";
    public static final String PREFIX = "api/";
    public static String SLUG = "";

    public static final String DEFAULT_TAG_JSON_ARRAY="result";
    public static final String DATA_TAG_JSON_ARRAY="data";
    public static final String CATEGORIES_TAG_JSON_ARRAY="categories";
    public static final String SELECTED_CATEGORY_TAG_JSON_ARRAY="selected_category";
    public static final String UNSELECTED_CATEGORY_TAG_JSON_ARRAY="unselected_category";
    public static final String PRODUCTS_TAG_JSON_ARRAY="products";
    public static final String TOKEN_TAG_JSON_ARRAY="token";
    public static final String SUCCESS_TAG_JSON_ARRAY="success";
    public static final String ERROR_TAG_JSON_ARRAY="error";

    public static final String POST_Login = PROTOCOL+HOST+PREFIX+"login";
    public static final String POST_Logout = PROTOCOL+HOST+PREFIX+"logout";
    public static final String POST_Reset_Password = PROTOCOL+HOST+PREFIX+"reset-password";
    public static final String POST_Change_Password = PROTOCOL+HOST+PREFIX+"change-password";

    public static final String GET_Product = PROTOCOL+HOST+PREFIX+"product";
    public static final String GET_Search_Product = PROTOCOL+HOST+PREFIX+"search-product";
    public static final String POST_ADD_Product = PROTOCOL+HOST+PREFIX+"add-product";
    public static final String GET_EDIT_Product = PROTOCOL+HOST+PREFIX+"edit-product";
    public static final String POST_EDIT_Product = PROTOCOL+HOST+PREFIX+"edit-product";
    public static final String POST_DELETE_Product = PROTOCOL+HOST+PREFIX+"delete-product";

    public static final String GET_Payment = PROTOCOL+HOST+PREFIX+"payment";
    public static final String POST_ADD_Payment = PROTOCOL+HOST+PREFIX+"add-payment";
    public static final String GET_EDIT_Payment = PROTOCOL+HOST+PREFIX+"edit-payment";
    public static final String POST_EDIT_Payment = PROTOCOL+HOST+PREFIX+"edit-payment";
    public static final String POST_DELETE_Payment = PROTOCOL+HOST+PREFIX+"delete-payment";

    public static final String GET_Retur = PROTOCOL+HOST+PREFIX+"retur";
    public static final String POST_ADD_Retur = PROTOCOL+HOST+PREFIX+"add-retur";
    public static final String GET_EDIT_Retur = PROTOCOL+HOST+PREFIX+"edit-retur";
    public static final String POST_EDIT_Retur = PROTOCOL+HOST+PREFIX+"edit-retur";
    public static final String POST_DELETE_Retur = PROTOCOL+HOST+PREFIX+"delete-retur";

    public static final String GET_Category = PROTOCOL+HOST+PREFIX+"category";
    public static final String GET_SEARCH_Category = PROTOCOL+HOST+PREFIX+"search-category";
    public static final String POST_ADD_Category = PROTOCOL+HOST+PREFIX+"add-category";
    public static final String GET_EDIT_Category = PROTOCOL+HOST+PREFIX+"edit-category";
    public static final String POST_EDIT_Category = PROTOCOL+HOST+PREFIX+"edit-category";
    public static final String POST_DELETE_Category = PROTOCOL+HOST+PREFIX+"delete-category";

    public static final String GET_User_Pending = PROTOCOL+HOST+PREFIX+"user-pending";
    public static final String GET_User_Approved = PROTOCOL+HOST+PREFIX+"user-approved";
    public static final String GET_SEARCH_User = PROTOCOL+HOST+PREFIX+"search-user";
    public static final String POST_ADD_User = PROTOCOL+HOST+PREFIX+"add-user";
    public static final String GET_EDIT_User = PROTOCOL+HOST+PREFIX+"edit-user";
    public static final String POST_EDIT_User = PROTOCOL+HOST+PREFIX+"edit-user";
    public static final String POST_DELETE_User = PROTOCOL+HOST+PREFIX+"delete-user";
    public static final String POST_APPROVE_User = PROTOCOL+HOST+PREFIX+"approve-user";
    public static final String POST_REJECT_User = PROTOCOL+HOST+PREFIX+"reject-user";
}
