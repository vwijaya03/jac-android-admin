package com.ktbi.jac.Setting;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.ktbi.jac.Activity.AddCategoryActivity;
import com.ktbi.jac.Activity.AddPaymentActivity;
import com.ktbi.jac.Activity.AddProductActivity;
import com.ktbi.jac.Activity.AddReturActivity;
import com.ktbi.jac.Activity.CategoryActivity;
import com.ktbi.jac.Activity.ChangePasswordActivity;
import com.ktbi.jac.Activity.CheckoutActivity;
import com.ktbi.jac.Activity.EditCategoryActivity;
import com.ktbi.jac.Activity.EditProductActivity;
import com.ktbi.jac.Activity.PaymentActivity;
import com.ktbi.jac.Activity.ProductActivity;
import com.ktbi.jac.Activity.ProductDetailActivity;
import com.ktbi.jac.Activity.ReturActivity;
import com.ktbi.jac.Activity.UserActivity;
import com.ktbi.jac.Activity.UserLoginOrSignupActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vikowijaya on 9/3/17.
 */

public class Logout extends AsyncTask<String, String, String>
{
    private Context mContext;
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;
    ProgressDialog pdLoading;
    HttpURLConnection conn;
    URL url = null;

    public Logout(ProductActivity productActivity)
    {
        this.mContext = productActivity;
        this.pdLoading = new ProgressDialog(productActivity);
    }

    public Logout(CheckoutActivity checkoutActivity) {
        this.mContext = checkoutActivity;
        this.pdLoading = new ProgressDialog(checkoutActivity);
    }

    public Logout(ProductDetailActivity productDetailActivity) {
        this.mContext = productDetailActivity;
        this.pdLoading = new ProgressDialog(productDetailActivity);
    }

    public Logout(PaymentActivity paymentActivity) {
        this.mContext = paymentActivity;
        this.pdLoading = new ProgressDialog(paymentActivity);
    }

    public Logout(ReturActivity returActivity) {
        this.mContext = returActivity;
        this.pdLoading = new ProgressDialog(returActivity);
    }

    public Logout(AddPaymentActivity addPaymentActivity) {
        this.mContext = addPaymentActivity;
        this.pdLoading = new ProgressDialog(addPaymentActivity);
    }

    public Logout(AddReturActivity addReturActivity) {
        this.mContext = addReturActivity;
        this.pdLoading = new ProgressDialog(addReturActivity);
    }

    public Logout(UserActivity userActivity) {
        this.mContext = userActivity;
        this.pdLoading = new ProgressDialog(userActivity);
    }

    public Logout(CategoryActivity categoryActivity) {
        this.mContext = categoryActivity;
        this.pdLoading = new ProgressDialog(categoryActivity);
    }

    public Logout(AddCategoryActivity addCategoryActivity) {
        this.mContext = addCategoryActivity;
        this.pdLoading = new ProgressDialog(addCategoryActivity);
    }

    public Logout(EditCategoryActivity editCategoryActivity) {
        this.mContext = editCategoryActivity;
        this.pdLoading = new ProgressDialog(editCategoryActivity);
    }

    public Logout(AddProductActivity addProductActivity) {
        this.mContext = addProductActivity;
        this.pdLoading = new ProgressDialog(addProductActivity);
    }

    public Logout(EditProductActivity editProductActivity) {
        this.mContext = editProductActivity;
        this.pdLoading = new ProgressDialog(editProductActivity);
    }

    public Logout(ChangePasswordActivity changePasswordActivity) {
        this.mContext = changePasswordActivity;
        this.pdLoading = new ProgressDialog(changePasswordActivity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        //this method will be running on UI thread
        pdLoading.setMessage("\tLoading...");
        pdLoading.setCancelable(false);
        pdLoading.show();

    }
    @Override
    protected String doInBackground(String... params) {
        try {

            // Enter URL address where your php file resides
            url = new URL(Config.POST_Logout);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "exception";
        }
        try {
            SharedPreferences preferences = this.mContext.getSharedPreferences("AuthUser", MODE_PRIVATE);

            Log.i("isi token", preferences.getString("AuthToken", ""));
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
            conn.setRequestMethod("POST");

            // setDoInput and setDoOutput method depict handling of both send and receive
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Append parameters to URL
//            Uri.Builder builder = new Uri.Builder();
//            String query = builder.build().getEncodedQuery();
//
//            // Open connection for sending data
//            OutputStream os = conn.getOutputStream();
//            BufferedWriter writer = new BufferedWriter(
//                    new OutputStreamWriter(os, "UTF-8"));
//            writer.write(query);
//            writer.flush();
//            writer.close();
//            os.close();
            conn.connect();

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return "exception";
        }

        try {

            int response_code = conn.getResponseCode();

            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                // Pass data to onPostExecute method
                return(result.toString().trim());

            } else if (response_code == 401) {

                return("token_invalid");

            }else if (response_code == 400) {

                return("token_invalid");

            } else{
                return("unsuccessful");
            }

        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @Override
    protected void onPostExecute(String result) {

//        try {
//            JSONObject errorObject = new JSONObject(result);
//
//            if(errorObject.getString("error") != null)
//            {
//                pdLoading.dismiss();
//
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
//
//                alertDialog.setTitle("Error");
//                alertDialog.setMessage("Sesi anda sudah habis !");
//                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
//                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        SharedPreferences preferences = mContext.getSharedPreferences("AuthUser",MODE_PRIVATE);
//                        preferences.edit().clear().apply();
//
//                        Intent i = new Intent(mContext, UserLoginOrSignupActivity.class);
//                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        mContext.startActivity(i);
//                    }
//                });
//
//                alertDialog.show();
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        //this method will be running on UI thread
        pdLoading.dismiss();

        if (result.equalsIgnoreCase("false")){

            // If username and password does not match display a error message
            Toast.makeText(mContext, "Invalid email or password", Toast.LENGTH_LONG).show();

        } else if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {

            Toast.makeText(mContext, "OOPs! Something went wrong. Connection Problem.", Toast.LENGTH_LONG).show();
        }else if (result.equalsIgnoreCase("token_invalid")) {

            SharedPreferences preferences = this.mContext.getSharedPreferences("AuthUser",MODE_PRIVATE);
            preferences.edit().clear().apply();

            SharedPreferences preferences2 = this.mContext.getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
            preferences2.edit().clear().apply();

            Intent i = new Intent(mContext, UserLoginOrSignupActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mContext.startActivity(i);
        }
        else
        {
            SharedPreferences preferences = this.mContext.getSharedPreferences("AuthUser",MODE_PRIVATE);
            preferences.edit().clear().apply();

            SharedPreferences preferences2 = this.mContext.getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
            preferences2.edit().clear().apply();

            Intent i = new Intent(mContext, UserLoginOrSignupActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mContext.startActivity(i);
        }
    }

}
