package com.ktbi.jac.TabFragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ktbi.jac.Activity.UserLoginOrSignupActivity;
import com.ktbi.jac.Adapter.ListviewAdapterUser;
import com.ktbi.jac.Model.UserModel;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.EndlessRecyclerViewLinearLayoutScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vikowijaya on 9/9/17.
 */


public class TabFragmentUserApprove extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private View view;
    private RecyclerView.Adapter recyclerViewUserAdapter;
    private ProgressDialog progressDialog;

    private List<UserModel> mDataList = new ArrayList<UserModel>();
    private EndlessRecyclerViewLinearLayoutScrollListener scrollListener;

    private int next_page = 0;
    private String search_user_name;

    private SharedPreferences preferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_pending, container, false);
        setHasOptionsMenu(true);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewUserPending);
        recyclerView.setHasFixedSize(false);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        scrollListener = new EndlessRecyclerViewLinearLayoutScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                if (mDataList.size() > 0) {
                    UserModel item = mDataList.get(mDataList.size() - 1);
                    if(item.isSpinner()){
                        getData(page);
                    }
                }
            }
        };
        recyclerView.addOnScrollListener(scrollListener);

        getData(0);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.search_product_item);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("Cari User...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                search_user_name = query;
                next_page = 0;
                mDataList.clear();
                getData(0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null)
                {
                    if(newText.length() == 0)
                    {
                        search_user_name = "";
                        next_page = 0;
                        mDataList.clear();
                        getData(0);
                    }
                }
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        return false;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    /*MenuItem item;
    menu.clear();
    getActivity().getMenuInflater().inflate(R.menu.search, menu);
    SearchView searchView=(SearchView)menu.findItem(R.id.search);
    searchView.setIconifiedByDefault(true);*/
    }

    private void getData(int page){
        class GetData extends AsyncTask<Void,Void,String> {
            public int page = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(getActivity(), "Loading", "Loading...",false,false);
                progressDialog.setCancelable(false);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                if(s == null)
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                    alertDialog.setTitle("Info");
                    alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    });

                    alertDialog.show();
                }
                else if(s.equalsIgnoreCase("401"))
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Sesi login anda sudah habis !");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences preferences = getActivity().getSharedPreferences("AuthUser",MODE_PRIVATE);
                            preferences.edit().clear().apply();

                            Intent i = new Intent(getActivity(), UserLoginOrSignupActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        }
                    });

                    alertDialog.show();
                }
                else
                {
                    parseJSON(next_page, s);
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                BufferedReader bufferedReader = null;
                try {
                    URL url;
                    if(next_page == 0)
                    {
                        url = new URL(Config.GET_SEARCH_User);
                    }
                    else
                    {
                        url = new URL(Config.GET_SEARCH_User+"?page="+next_page);
                    }

                    SharedPreferences preferences = getActivity().getSharedPreferences("AuthUser", MODE_PRIVATE);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                    con.addRequestProperty("name", search_user_name);
                    con.addRequestProperty("status", "1");
                    con.setRequestMethod("GET");

                    int response_code = con.getResponseCode();

                    if(response_code == HttpURLConnection.HTTP_UNAUTHORIZED)
                    {
                        return String.valueOf(response_code);
                    }

                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                } catch (SocketTimeoutException e){
                    return null;
                } catch(Exception e){
                    return null;
                }
            }
        }
        GetData gd = new GetData();
//        gd.page = page;
        gd.execute();
    }

    private void parseJSON(int page, String json){
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONObject resultUserObject = jsonObject.getJSONObject(Config.DEFAULT_TAG_JSON_ARRAY);
            JSONArray arrayUser = resultUserObject.getJSONArray(Config.DATA_TAG_JSON_ARRAY);

            if (next_page == 0) {
                this.mDataList.removeAll(this.mDataList);
                recyclerViewUserAdapter = new ListviewAdapterUser(this.mDataList, "ya");
                recyclerView.setAdapter(recyclerViewUserAdapter);
            } else {
                if (this.mDataList.size() > 0) {
                    this.mDataList.remove(this.mDataList.size() - 1);
                }
            }

            if(arrayUser != null)
            {
                for(int i=0; i < arrayUser.length(); i++){
                    UserModel item = new UserModel();
                    JSONObject j = arrayUser.getJSONObject(i);
                    item.setId(getId(j));
                    item.setFullname(getFullname(j));
                    item.setEmail(getEmail(j));
                    item.setKota(j.getString("username"));
                    item.setKecamatan(getKecamatan(j));
                    item.setKode_pos(getKodePos(j));
                    item.setTelepon(getTelepon(j));
                    item.setTipe(getTipe(j));
                    Log.i("cek", getTipe(j));
                    this.mDataList.add(item);
                }
            }

            if(resultUserObject.isNull("to"))
            {
                Log.i("masuk to = null", "masuk to = null");
                this.mDataList.clear();
                this.next_page = 0;
            }
            else if(resultUserObject.getString("to") != null)
            {
                Log.i("masuk if to not null", "nja");
                if(Integer.parseInt(resultUserObject.getString("to")) < Integer.parseInt(resultUserObject.getString("total"))){
                    this.next_page = Integer.parseInt(resultUserObject.getString("current_page"))+1;

                    UserModel item = new UserModel();
                    item.setId("");
                    item.setFullname("");
                    item.setEmail("");
                    item.setKota("");
                    item.setSpinner(true);
                    this.mDataList.add(item);
                }
            }
            recyclerViewUserAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getId(JSONObject j){
        String id = null;
        try {
            id = j.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    private String getFullname(JSONObject j){
        String value = null;
        try {
            value = j.getString("fullname");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getEmail(JSONObject j){
        String value = null;
        try {
            value = j.getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getTelepon(JSONObject j){
        String value = null;
        try {
            value = j.getString("telepon");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getKota(JSONObject j){
        String value = null;
        try {
            value = j.getString("kota");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getKecamatan(JSONObject j){
        String value = null;
        try {
            value = j.getString("kecamatan");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getKodePos(JSONObject j){
        String value = null;
        try {
            value = j.getString("kode_pos");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getTipe(JSONObject j){
        String value = null;
        try {
            value = j.getString("tipe");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

}