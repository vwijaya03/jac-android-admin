package com.ktbi.jac.TabFragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ktbi.jac.Activity.UserLoginOrSignupActivity;
import com.ktbi.jac.Adapter.ListviewAdapterCategory;
import com.ktbi.jac.Adapter.ListviewAdapterUser;
import com.ktbi.jac.Model.CategoryProductModel;
import com.ktbi.jac.Model.UserModel;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.EndlessRecyclerViewLinearLayoutScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vikowijaya on 9/9/17.
 */

public class TabFragmentCategory extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private View view;
    private RecyclerView.Adapter recyclerViewUserAdapter;
    private ProgressDialog progressDialog;

    private List<CategoryProductModel> mDataList = new ArrayList<CategoryProductModel>();
    private EndlessRecyclerViewLinearLayoutScrollListener scrollListener;

    private int next_page = 0;
    private String search_category_name;

    private SharedPreferences preferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category, container, false);
        setHasOptionsMenu(true);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewCategory);
        recyclerView.setHasFixedSize(false);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        scrollListener = new EndlessRecyclerViewLinearLayoutScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                if (mDataList.size() > 0) {
                    CategoryProductModel item = mDataList.get(mDataList.size() - 1);
                    if(item.isSpinner()){
                        getData(page);
                    }
                }
            }
        };
        recyclerView.addOnScrollListener(scrollListener);

        getData(0);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.search_product_item);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("Cari Data Kategori...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                search_category_name = query;
                next_page = 0;
                mDataList.clear();
                getData(0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null)
                {
                    if(newText.length() == 0)
                    {
                        search_category_name = "";
                        next_page = 0;
                        mDataList.clear();
                        getData(0);
                    }
                }
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        return false;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    /*MenuItem item;
    menu.clear();
    getActivity().getMenuInflater().inflate(R.menu.search, menu);
    SearchView searchView=(SearchView)menu.findItem(R.id.search);
    searchView.setIconifiedByDefault(true);*/
    }

    private void getData(int page){
        class GetData extends AsyncTask<Void,Void,String> {
            public int page = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(getActivity(), "Loading", "Loading...",false,false);
                progressDialog.setCancelable(false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                if(s == null)
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                    alertDialog.setTitle("Info");
                    alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    });

                    alertDialog.show();
                }
                else if(s.equalsIgnoreCase("401"))
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Sesi login anda sudah habis !");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences preferences = getActivity().getSharedPreferences("AuthUser",MODE_PRIVATE);
                            preferences.edit().clear().apply();

                            Intent i = new Intent(getActivity(), UserLoginOrSignupActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        }
                    });

                    alertDialog.show();
                }
                else
                {
                    parseJSON(next_page, s);
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                BufferedReader bufferedReader = null;
                try {
                    URL url;
                    if(next_page == 0)
                    {
                        url = new URL(Config.GET_SEARCH_Category);
                    }
                    else
                    {
                        url = new URL(Config.GET_SEARCH_Category+"?page="+next_page);
                    }

                    SharedPreferences preferences = getActivity().getSharedPreferences("AuthUser", MODE_PRIVATE);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                    con.addRequestProperty("category_name", search_category_name);
                    con.setRequestMethod("GET");

                    int response_code = con.getResponseCode();

                    if(response_code == HttpURLConnection.HTTP_UNAUTHORIZED)
                    {
                        return String.valueOf(response_code);
                    }

                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                } catch (SocketTimeoutException e){
                    return null;
                } catch(Exception e){
                    return null;
                }
            }
        }
        GetData gd = new GetData();
//        gd.page = page;
        gd.execute();
    }

    private void parseJSON(int page, String json){
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONObject resultCategoryObject = jsonObject.getJSONObject(Config.DEFAULT_TAG_JSON_ARRAY);
            JSONArray arrayCategory = resultCategoryObject.getJSONArray(Config.DATA_TAG_JSON_ARRAY);

            if (next_page == 0) {
                this.mDataList.removeAll(this.mDataList);
                recyclerViewUserAdapter = new ListviewAdapterCategory(this.mDataList);
                recyclerView.setAdapter(recyclerViewUserAdapter);
            } else {
                if (this.mDataList.size() > 0) {
                    this.mDataList.remove(this.mDataList.size() - 1);
                }
            }

            if(arrayCategory != null)
            {
                for(int i=0; i < arrayCategory.length(); i++){
                    CategoryProductModel item = new CategoryProductModel();
                    JSONObject j = arrayCategory.getJSONObject(i);
                    item.setId(getId(j));
                    item.setName(getCategoryName(j));
                    this.mDataList.add(item);
                }
            }

            if(resultCategoryObject.isNull("to"))
            {
                Log.i("masuk to = null", "masuk to = null");
                this.mDataList.clear();
                this.next_page = 0;
            }
            else if(resultCategoryObject.getString("to") != null)
            {
                Log.i("masuk if to not null", "nja");
                if(Integer.parseInt(resultCategoryObject.getString("to")) < Integer.parseInt(resultCategoryObject.getString("total"))){
                    this.next_page = Integer.parseInt(resultCategoryObject.getString("current_page"))+1;

                    CategoryProductModel item = new CategoryProductModel();
                    item.setId("");
                    item.setName("");
                    item.setSpinner(true);
                    this.mDataList.add(item);
                }
            }
            recyclerViewUserAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getId(JSONObject j){
        String id = null;
        try {
            id = j.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    private String getCategoryName(JSONObject j){
        String value = null;
        try {
            value = j.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

}
