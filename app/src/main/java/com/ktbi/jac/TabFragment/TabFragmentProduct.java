package com.ktbi.jac.TabFragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ktbi.jac.Activity.ProductActivity;
import com.ktbi.jac.Activity.UserLoginOrSignupActivity;
import com.ktbi.jac.Adapter.GridviewAdapterProduct;
import com.ktbi.jac.Model.CategoryProductModel;
import com.ktbi.jac.Model.ProductModel;
import com.ktbi.jac.R;
import com.ktbi.jac.Setting.Config;
import com.ktbi.jac.Setting.EndlessRecyclerViewGrirdLayoutScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by apple on 18/03/16.
 */
public class TabFragmentProduct extends Fragment {

    private RecyclerView recyclerView;
    private View view;
    private RecyclerView.Adapter recyclerViewProductAdapter;
    private ProgressDialog progressDialog;

    private List<ProductModel> mDataList = new ArrayList<ProductModel>();
    private EndlessRecyclerViewGrirdLayoutScrollListener scrollListener;

    private ArrayList<CategoryProductModel> arrayListCategoryProduct = new ArrayList<CategoryProductModel>();
    private ArrayAdapter<CategoryProductModel> arrayAdapterCategoryProduct;

    private Spinner spinner;
    private boolean isFirstSelection = false;
    private int next_page = 0;
    private String search_product_value;
    private String category_id;
    private String sSelectedCategory;

    private SharedPreferences preferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product, container, false);
        setHasOptionsMenu(true);

        //Add countries
//        arrayListCategoryProduct.add(new CategoryProductModel("1", "India"));

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewProduct);
        //recyclerView.setHasFixedSize(false);

        GridLayoutManager glm = new GridLayoutManager(getActivity(), 2);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(glm);

        scrollListener = new EndlessRecyclerViewGrirdLayoutScrollListener((GridLayoutManager) glm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                if (mDataList.size() > 0) {
                    ProductModel item = mDataList.get(mDataList.size() - 1);
                    if(item.isSpinner()){
                        getData(page);
                    }
                }
            }
        };
        recyclerView.addOnScrollListener(scrollListener);

        spinner = (Spinner) view.findViewById(R.id.category_product_spinner);
        //materialBetterSpinner.setSelection(adapter.getPosition());
        arrayListCategoryProduct.add(new CategoryProductModel("all", "Semua Kategori Produk"));
        arrayAdapterCategoryProduct = new ArrayAdapter<CategoryProductModel>(view.getContext(), android.R.layout.simple_dropdown_item_1line, arrayListCategoryProduct);

        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isFirstSelection = true;
                return false;
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(isFirstSelection)
                {
                    Log.i("if first selection", "masuk if first ");
                    isFirstSelection = false;
                    if(arrayListCategoryProduct.get(i).getId().equalsIgnoreCase("all"))
                    {
                        category_id = "";
                    }
                    else
                    {
                        category_id = arrayListCategoryProduct.get(i).getId();
                    }

                    preferences = getActivity().getSharedPreferences("SelectedCategoryProduct", MODE_PRIVATE);
                    preferences.edit().putInt("SelectedCategoryProductValue", i).apply();

                    next_page = 0;


                    arrayListCategoryProduct.add(new CategoryProductModel("all", "Semua Kategori Produk"));
                    //arrayAdapterCategoryProduct.notifyDataSetChanged();
                    getData(0);

                }
                else
                {
                    Log.i("else selection", "masuk else");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getData(0);

     return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.search_product_item);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("Cari Produk...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                search_product_value = query;
                next_page = 0;

                arrayListCategoryProduct.add(new CategoryProductModel("all", "Semua Kategori Produk"));
                getData(0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null)
                {
                    if(newText.length() == 0)
                    {
                        preferences = getActivity().getSharedPreferences("SelectedCategoryProduct", MODE_PRIVATE);
                        preferences.edit().clear().apply();
                        category_id = "";
                        search_product_value = "";
                        next_page = 0;

                        getData(0);
                    }
                }
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        return false;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        /*MenuItem item;
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.search, menu);
        SearchView searchView=(SearchView)menu.findItem(R.id.search);
        searchView.setIconifiedByDefault(true);*/
    }

    private void getData(int page){
        class GetData extends AsyncTask<Void,Void,String> {
            public int page = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(getActivity(), "Loading", "Loading...",false,false);
                progressDialog.setCancelable(false);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                if(s == null)
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                    alertDialog.setTitle("Info");
                    alertDialog.setMessage("Koneksi ke server terputus, tolong coba lagi !");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(getActivity(), ProductActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            SharedPreferences preferences2 = getActivity().getSharedPreferences("SelectedCategoryProduct",MODE_PRIVATE);
                            preferences2.edit().clear().apply();
                            startActivity(i);
                        }
                    });

                    alertDialog.show();
                }
                else if(s.equalsIgnoreCase("401"))
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Sesi login anda sudah habis !");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences preferences = getActivity().getSharedPreferences("AuthUser",MODE_PRIVATE);
                            preferences.edit().clear().apply();

                            Intent i = new Intent(getActivity(), UserLoginOrSignupActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        }
                    });

                    alertDialog.show();
                }
                else
                {
                    parseJSON(next_page, s);
                    preferences = getActivity().getSharedPreferences("SelectedCategoryProduct", MODE_PRIVATE);
                    spinner.setSelection(preferences.getInt("SelectedCategoryProductValue", 0));
                }

            }

            @Override
            protected String doInBackground(Void... params) {
                BufferedReader bufferedReader = null;
                try {
                    URL url;
                    Log.i("isi next page", String.valueOf(next_page));
                    if(next_page == 0)
                    {
                        url = new URL(Config.GET_Search_Product);
                    }
                    else
                    {
                        url = new URL(Config.GET_Search_Product+"?page="+next_page);
                    }

                    SharedPreferences preferences = getActivity().getSharedPreferences("AuthUser", MODE_PRIVATE);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.addRequestProperty("Authorization", preferences.getString("AuthToken", ""));
                    con.addRequestProperty("title", search_product_value);
                    con.addRequestProperty("category_id", category_id);
                    con.setRequestMethod("GET");

                    int response_code = con.getResponseCode();

                    if(response_code == HttpURLConnection.HTTP_UNAUTHORIZED)
                    {
                        return String.valueOf(response_code);
                    }

                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while((json = bufferedReader.readLine())!= null){
                        sb.append(json+"\n");
                    }

                    return sb.toString().trim();

                } catch (SocketTimeoutException e){
                    return null;
                } catch(Exception e){
                    return null;
                }
            }
        }
        GetData gd = new GetData();
//        gd.page = page;
        gd.execute();
    }

    private void parseJSON(int page, String json){
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONObject resultProductObject = jsonObject.getJSONObject(Config.DEFAULT_TAG_JSON_ARRAY);
            JSONArray arrayProduct = resultProductObject.getJSONArray(Config.DATA_TAG_JSON_ARRAY);
            JSONArray arrayCategoryProduct = jsonObject.getJSONArray(Config.CATEGORIES_TAG_JSON_ARRAY);
            JSONObject jsonObjects = new JSONObject(json);

            if (next_page == 0) {
                this.mDataList.removeAll(this.mDataList);
                this.arrayListCategoryProduct.removeAll(this.arrayListCategoryProduct);
                arrayListCategoryProduct.add(new CategoryProductModel("all", "Semua Kategori Produk"));
                recyclerViewProductAdapter = new GridviewAdapterProduct(this.mDataList);
                recyclerView.setAdapter(recyclerViewProductAdapter);
            } else {
                this.arrayListCategoryProduct.removeAll(this.arrayListCategoryProduct);
                arrayListCategoryProduct.add(new CategoryProductModel("all", "Semua Kategori Produk"));
                if (this.mDataList.size() > 0) {
                    this.mDataList.remove(this.mDataList.size() - 1);
                }
            }

            if(arrayProduct != null)
            {
                for(int i=0; i < arrayProduct.length(); i++){
                    ProductModel item = new ProductModel();
                    JSONObject j = arrayProduct.getJSONObject(i);
                    item.setId(getId(j));
                    item.setCategory_id(getCategoryProductIdFromProduct(j));
                    item.setCategory_name(getCategoryName(j));
                    item.setTitle(getTitle(j));
                    item.setDescription(getDescription(j));
                    item.setImg_path1(Config.PROTOCOL+Config.HOST+getImgPath1(j));
                    item.setImg_path2(getImgPath2(j));
                    item.setImg_path3(getImgPath3(j));
                    item.setImg_path4(getImgPath4(j));
                    this.mDataList.add(item);
                }
            }

            for(int m=0; m < arrayCategoryProduct.length(); m++){
                JSONObject n = arrayCategoryProduct.getJSONObject(m);
                this.arrayListCategoryProduct.add(new CategoryProductModel(getCategoryProductId(n), getCategoryProductName(n)));
            }

            arrayAdapterCategoryProduct = new ArrayAdapter<CategoryProductModel>(view.getContext(), android.R.layout.simple_dropdown_item_1line, arrayListCategoryProduct);
            spinner.setAdapter(arrayAdapterCategoryProduct);

            if(resultProductObject.isNull("to"))
            {
                Log.i("masuk to = null", "masuk to = null");
                this.mDataList.clear();
                this.next_page = 0;
            }
            else if(resultProductObject.getString("to") != null)
            {
                Log.i("masuk if to not null", "nja");
                if(Integer.parseInt(resultProductObject.getString("to")) < Integer.parseInt(resultProductObject.getString("total"))){
                    this.next_page = Integer.parseInt(resultProductObject.getString("current_page"))+1;
                    Log.i("isi next page di result", String.valueOf(this.next_page));
                    ProductModel item = new ProductModel();
                    item.setId("");
                    item.setCategory_name("");
                    item.setTitle("");
                    item.setDescription("");
                    item.setSpinner(true);
                    this.mDataList.add(item);
                }
            }

//            for (int i=0; i < arrayListCategoryProduct.size(); i++)
//            {
//                Log.i("array list id", arrayListCategoryProduct.get(i).getId());
//                if(arrayListCategoryProduct.get(i).getId().equals(sSelectedCategory))
//                {
//                    Log.i("masuk if", "masuk if");
//                    spinner.setSelection(i);
//                    category_id = arrayListCategoryProduct.get(i).getId();
//                    break;
//                }
//            }

            recyclerViewProductAdapter.notifyDataSetChanged();
            arrayAdapterCategoryProduct.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getCategoryProductId(JSONObject j){
        String id = null;
        try {
            id = j.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    private String getCategoryProductIdFromProduct(JSONObject j){
        String id = null;
        try {
            id = j.getString("category_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    private String getCategoryProductName(JSONObject j){
        String name = null;
        try {
            name = j.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return name;
    }

    private String getId(JSONObject j){
        String id = null;
        try {
            id = j.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    private String getCategoryName(JSONObject j){
        String category_name = null;
        try {
            category_name = j.getString("category_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return category_name;
    }

    private String getTitle(JSONObject j){
        String title = null;
        try {
            title = j.getString("title");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return title;
    }

    private String getDescription(JSONObject j){
        String description = null;
        try {
            description = j.getString("description");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return description;
    }

    private String getImgPath1(JSONObject j){
        String img_path1 = null;
        try {
            img_path1 = j.getString("img_path1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return img_path1;
    }

    private String getImgPath2(JSONObject j){
        String img_path2 = null;
        try {
            img_path2 = j.getString("img_path2");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return img_path2;
    }

    private String getImgPath3(JSONObject j){
        String img_path3 = null;
        try {
            img_path3 = j.getString("img_path3");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return img_path3;
    }

    private String getImgPath4(JSONObject j){
        String img_path4 = null;
        try {
            img_path4 = j.getString("img_path4");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return img_path4;
    }
}