package com.ktbi.jac;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ktbi.jac.Model.CheckoutConfirmationModel;

import java.util.ArrayList;

public class TabFragmentCheckout3 extends Fragment {

    private  View view;

    private ListView listview;

    Typeface fonts1,fonts2;

    private int[] IMAGE = {R.drawable.box, R.drawable.ball, R.drawable.bag,
            R.drawable.box, R.drawable.ball};

    private String[] TITLE = {"Teak & Steel Petanque Set", "Lemon Peel Baseball", "Seil Marschall Hiking Pack", "Teak & Steel Petanque Set", "Lemon Peel Baseball"};

    private String[] DESCRIPTION = {"One Size", "One Size", "Size L", "One Size", "One Size"};

    private String[] DATE = {"$ 220.00","$ 49.00","$ 320.00","$ 220.00","$ 49.00"};

    private ArrayList<com.ktbi.jac.Model.CheckoutConfirmationModel> CheckoutConfirmationModel;
    private CheckoutConfirmationAdapter baseAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.checkout_fragmenttab3, container, false);
        listview = (ListView)view.findViewById(R.id.listview);
        CheckoutConfirmationModel = new ArrayList<CheckoutConfirmationModel>();

        for (int i= 0; i< TITLE.length; i++){

            CheckoutConfirmationModel checkoutConfirmationModel = new CheckoutConfirmationModel(IMAGE[i], TITLE[i], DESCRIPTION[i], DATE[i]);
            CheckoutConfirmationModel.add(checkoutConfirmationModel);

        }

        baseAdapter = new CheckoutConfirmationAdapter(getActivity(), CheckoutConfirmationModel) {
        };

        listview.setAdapter(baseAdapter);


//
//        fonts1 =  Typeface.createFromAsset(TabFragmentCheckout3.this.getAssets(),
//                "fonts/Lato-Light.ttf");
//
//
//
//        fonts2 =  Typeface.createFromAsset(TabFragmentCheckout3.this.getAssets(),
//                "fonts/Lato-Regular.ttf");
//
//
//
//
//        TextView t4 =(TextView)findViewById(R.id.shopping);
//        t4.setTypeface(fonts2);
//        TextView t5 =(TextView)findViewById(R.id.pay);
//        t5.setTypeface(fonts1);

        return  view;

    }
}