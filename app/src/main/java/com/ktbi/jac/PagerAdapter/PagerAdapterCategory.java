package com.ktbi.jac.PagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ktbi.jac.TabFragment.TabFragmentCategory;
import com.ktbi.jac.TabFragment.TabFragmentProduct;

/**
 * Created by vikowijaya on 9/9/17.
 */

public class PagerAdapterCategory extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterCategory(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabFragmentCategory tab1 = new TabFragmentCategory();
                return tab1;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}