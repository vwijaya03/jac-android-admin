package com.ktbi.jac.PagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ktbi.jac.TabFragmentCheckout1;
import com.ktbi.jac.TabFragmentCheckout2;
import com.ktbi.jac.TabFragmentCheckout3;

public class PagerAdapterCheckout extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterCheckout(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabFragmentCheckout1 tab1 = new TabFragmentCheckout1();
                return tab1;
            case 1:
                TabFragmentCheckout2 tab2 = new TabFragmentCheckout2();
                return tab2;
            case 2:
                TabFragmentCheckout3 tab3 = new TabFragmentCheckout3();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}