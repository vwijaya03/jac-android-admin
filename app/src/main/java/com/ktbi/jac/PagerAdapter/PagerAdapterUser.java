package com.ktbi.jac.PagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ktbi.jac.TabFragment.TabFragmentUserApprove;
import com.ktbi.jac.TabFragment.TabFragmentUserPending;

/**
 * Created by vikowijaya on 9/9/17.
 */

public class PagerAdapterUser extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterUser(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        this.mNumOfTabs = tabCount;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabFragmentUserPending tab1 = new TabFragmentUserPending();
                return tab1;
            case 1:
                TabFragmentUserApprove tab2 = new TabFragmentUserApprove();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}