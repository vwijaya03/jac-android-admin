package com.ktbi.jac.PagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ktbi.jac.TabFragment.TabFragmentProduct;

public class PagerAdapterProduct extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterProduct(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabFragmentProduct tab1 = new TabFragmentProduct();
                return tab1;
//            case 1:
//                TabFragmentCategory2 tab2 = new TabFragmentCategory2();
//                return tab2;
//            case 2:
//                TabFragmentCategory3 tab3 = new TabFragmentCategory3();
//                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}