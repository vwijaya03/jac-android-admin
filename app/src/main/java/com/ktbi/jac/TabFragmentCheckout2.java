package com.ktbi.jac;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.R.attr.startYear;

/**
 * Created by apple on 18/03/16.
 */
public class TabFragmentCheckout2 extends Fragment implements DatePickerDialog.OnDateSetListener {

    EditText displayDate;
    int mYear;
    int mMonth;
    int mDay;
    boolean isOkayClicked;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.checkout_fragmenttab2, null, false);
        displayDate = (EditText) view.findViewById(R.id.date2);
        final Calendar mcurrentDate=Calendar.getInstance();

        displayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final DatePickerDialog mDatePicker=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener()
                {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        mcurrentDate.set(selectedyear, selectedmonth, selectedday);
                        String date = new SimpleDateFormat("MM/dd/yyyy").format(mcurrentDate.getTime());
                        displayDate.setText(date);
                        Log.i("Cek", date);
                        mYear = mcurrentDate.get(Calendar.YEAR);
                        mMonth = mcurrentDate.get(Calendar.MONTH);
                        mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                    }
                },mYear, mMonth, mDay);

                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                Calendar d = Calendar.getInstance();

                mDatePicker.updateDate(d.get(Calendar.YEAR), d.get(Calendar.MONTH), d.get(Calendar.DAY_OF_MONTH));

                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }

        });

//        displayDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                DatePickerDialog mDatePicker=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener()
//                {
//                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
//
//                        String myFormat = "dd/MMM/yyyy"; //In which you need put here
//                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
//                        displayDate.setText(sdf.format(mcurrentDate.getTime()));
//                    }
//                },mYear, mMonth, mDay);
//
//                mDatePicker.setTitle("Select date");
//                mDatePicker.show();
//            }
//        });
        return view;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    }
}