package com.ktbi.jac;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ktbi.jac.Model.CheckoutConfirmationModel;

import java.util.ArrayList;


public class CheckoutConfirmationAdapter extends BaseAdapter {

    Context context;
    ArrayList<CheckoutConfirmationModel> checkoutConfirmationModel;
    Typeface fonts1,fonts2;

    public CheckoutConfirmationAdapter(Context context, ArrayList<CheckoutConfirmationModel> checkoutConfirmationModel) {
        this.context = context;
        this.checkoutConfirmationModel = checkoutConfirmationModel;
    }

    @Override
    public int getCount() {
        return checkoutConfirmationModel.size();
    }

    @Override
    public Object getItem(int position) {
        return checkoutConfirmationModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        fonts1 =  Typeface.createFromAsset(context.getAssets(),
                "fonts/MavenPro-Regular.ttf");

        ViewHolder viewHolder = null;

        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.checkout_list,null);

            viewHolder = new ViewHolder();

            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.discription = (TextView) convertView.findViewById(R.id.description);
            viewHolder.date = (TextView)convertView.findViewById(R.id.date);
//            viewHolder.min = (ImageView)convertView.findViewById(R.id.min);
            viewHolder.text = (TextView)convertView.findViewById(R.id.text);
//            viewHolder.plus = (ImageView)convertView.findViewById(R.id.plus);


            viewHolder.title.setTypeface(fonts1);
            viewHolder.discription.setTypeface(fonts1);
            viewHolder.text.setTypeface(fonts1);
            viewHolder.date.setTypeface(fonts1);

            convertView.setTag(viewHolder);

        }else {

            viewHolder = (ViewHolder)convertView.getTag();
        }

        CheckoutConfirmationModel checkoutConfirmationModel = (CheckoutConfirmationModel)getItem(position);

        viewHolder.image.setImageResource(checkoutConfirmationModel.getImage());
        viewHolder.title.setText(checkoutConfirmationModel.getTitle());
        viewHolder.discription.setText(checkoutConfirmationModel.getDiscription());
        viewHolder.date.setText(checkoutConfirmationModel.getDate());

//        number = 01;
//        viewHolder.text.setText(""+number);
//
//        final ViewHolder finalViewHolder = viewHolder;
//        viewHolder.min.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (number == 1){
//                    finalViewHolder.text.setText("" + number);
//            }
//
//                if (number > 1){
//
//                    number = number -1;
//                    finalViewHolder.text.setText(""+number);
//                }
//
//            }
//        });
//
//        final ViewHolder finalViewHolder1 = viewHolder;
//        viewHolder.plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (number == 10) {
//                    finalViewHolder1.text.setText("" + number);
//                }
//
//                if (number < 10) {
//
//                    number = number + 1;
//                    finalViewHolder1.text.setText("" + number);
//
//                }
//
//
//
//
//            }
//        });

        return convertView;
    }

    private class ViewHolder{
        ImageView image;
        TextView title;
        TextView discription;
        TextView date;
        ImageView min;
        TextView text;
        ImageView plus;
    }
}




